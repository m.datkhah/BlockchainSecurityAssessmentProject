<?php
// config for Question Answering System

// Website information
define('QA_TITLE', 'سامانه دستیار ارزیابی امنیتی زنجیره بلوکی(اتریوم)');
define('QA_TITLE_DESCRIPTION', 'این سامانه جهت تسهیل در روند ارزیابی امنیتی زنجیره بلوکی (اتریوم) طراحی شده است.');
define('QA_HOME_URL', 'http://localhost/OLD/project%20final%20edited/7learn2/');
define('QA_HARDWAREPAGE_URL', 'http://localhost/OLD/project%20final%20edited/7learn2/hardware-page.php#breadcrumbs');
define('QA_NETWORKPAGE_URL', 'http://localhost/OLD/project%20final%20edited/7learn2/network-page.php#breadcrumbs');
define('QA_SOFTWAREPAGE_URL', 'http://localhost/OLD/project%20final%20edited/7learn2/software-page.php#breadcrumbs');
define('QA_QUSETION_PER_PAGE', 50);
define('QA_QUSETION_MIN_LENGTH', 10);
define('QA_ANSWERS_MIN_LENGTH', 10);
define('QA_UNAME_MIN_LENGTH', 5);
define('QA_UNAMBER_MIN_LENGTH', ' ');
define('U_UPASSWORD_MIN_LENGTH', 5);
define('QA_UNAMBER_MAX_LENGTH', 3);
define('QA_DATE_FORMAT', "d F Y");

// admin information
define('QA_ADMIN_DISPLAYNAME', 'مدیر');
define('QA_ADMIN_USERNAME', 'admin');
define('QA_ADMIN_PASSWORD', 'e4d58a317f4e625124754e89ee8297897d2f125a');
define('ADMIN_ID', '2');
define('QA_ADMIN_UMAIL', 'admin@gmail.com');
define('QA_ADMIN_UMOBILE', '0938------2');

// user information
define('QA_USER_DISPLAYNAME', 'ارزیاب');

// turn off error reporting after project completion
ini_set('display_errors', 'On');
error_reporting(E_ALL);


// database information
$dbHost = 'localhost';
$dbUser = 'qa_db_user';
$dbPass = 'stez5TSvX959vhqz';
$dbName = 'project_qa';

$db = new mysqli($dbHost, $dbUser, $dbPass, $dbName);

/* check connection */
if ($db->connect_errno) {
    printf("Connect failed: %s\n", $db->connect_error);
    exit();
}
// for farsi data transfer to/from database
$db->query("SET NAMES UTF8;");

// define our tables for usage in code
$db->questionTable = "questions";
$db->answerTable = "answers";
$db->userTable = "users";
$db->categoriesTable = "categories";