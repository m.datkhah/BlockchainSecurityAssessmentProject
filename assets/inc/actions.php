<?php
include_once 'config.php';
include_once 'functions.php';


$errorMsg3 = false; // error message
$successMsg3 = false; // success message
if (isset($_POST['submitAnswered'])) {
    if (isValidQuestion($_POST['uAnswered'], $_POST['rid1'], $_POST['qid'], $errorMsg3)) {
        // add question to database ...
        if (addQuestion($_POST['uAnswered'], $_POST['rid1'], $_POST['qid'],$_POST['uid'], $errorMsg3)) {
            $successMsg3 = "پاسخ شما با موفقیت ثبت شد. ";
        }
    }
}

if (isset($_POST['SubmitAdminQuestion'])) {
    if (isValidAdminQuestion($_POST['AdminQuestion'], $_POST['rid3'], $_POST['view'], $errorMsg3)) {
        if (addQuestionAdmin($_POST['AdminQuestion'], $_POST['rid3'], $_POST['view'], $errorMsg3)) {
            $successMsg3 = "سوال شما با موفقیت ثبت شد. ";
        }
    }
}

if (isset($_POST['submitCategories'])) {
    if (isValidCategories($_POST['qid'], $_POST['rid2'], $_POST['Overlap_id'], $errorMsg3)) {
        if (addCategories($_POST['qid'], $_POST['rid2'], $_POST['Overlap_id'], $errorMsg3)) {
            $successMsg3 = "سوال هم پوشانی شما با موفقیت ثبت شد. ";
        }
    }
}
$errorMsg1 = false; // error message
$successMsg1 = false; // success message
if (isset($_POST['submitUsers'])) {
    if (isValidUsers($_POST['username1'], $_POST['password1'], $_POST['email'], $errorMsg1)) {
        // add question to database ...
        if (addUsers($_POST['username1'], $_POST['password1'], $_POST['email'], $errorMsg1)) {
            $successMsg1 = "ثبت نام با موفقیت انجام شد. ";
        }
    }
}
$errorMsg = false; // error message
$successMsg = false; // success message
if (isset($_POST['submitUsername'])) {
    if (isValidUserandAdmin($_POST['username2'], $_POST['password2'], $errorMsg)) {
        if (checkUserorAdmin($_POST['username2'],$successMsg)){

        }

    }
}


if (isset($_POST['submitAnswer']) and isAdmin()) {
    if (addAnswer($_POST['qid'], $_POST['text'], $errorMsg)) {
        $successMsg = "پاسخ با موفقیت ثبت شد .";
    }
}


if (isset($_POST['userLoginAllow']) and isAdmin()) {
    if (confirmationUser($_POST['userLoginAllow'], $errorMsg1)) {
        $successMsg1 = "ارزیاب با موفقیت تایید شد .";
    }
}
/*    "حذف پاسخ ها توسط مدیر و ارزیاب"*/

if (isset($_POST['removeAnswer'])) {
    // add question to database ...
    if (removeAnswer($_POST['text'], $errorMsg)) {
        $successMsg = "پاسخ انتخاب شده با موفقیت حذف شد. ";
    }
}

/*    "حذف پاسخ ها توسط مدیر"پایان*/
if (isset($_POST['login'])) {
    if (doLogin($_POST['username'], $_POST['password'])) {
        header("Location: " . QA_HOME_URL);
    } else {
        $errorMsg = 'نام کاربری یا رمز وارد شده اشتباه است .';
    }
}


if (isset($_GET['logout'])) {
    doLogout();
}

// get questions
$questions = null;
$numQuestions = 0;
$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
if (isset($_GET['search']) and strlen($_GET['search']) > 0) {
    /* a simple trick : replace space with % for use in sql Like statement
     * other aproach : like , fulltext , concat
     * Read this pages :  http://dev.mysql.com/doc/refman/5.0/en/fulltext-natural-language.html
                          http://www.mysqltutorial.org/mysql-full-text-search.aspx
     */
    $search = str_ireplace(' ', '%', $_GET['search']);
    if (isset($_GET['status']) and isset($_GET['search'] )) {
        $questions = getQuestions($_GET['status'], $search, $page, $numQuestions);
    } else {
        $questions = getQuestions('all', $search, $page, $numQuestions);
    }
}
  if (isset($_GET['numberQuestion']))
  {
      $questions= getNumberQuestions($_GET['numberQuestion']);
  }


  if( isset($_GET['search1'])){
      print_r($_GET['search1']);
      $questions= getCategories($_GET['search1']);

  }










