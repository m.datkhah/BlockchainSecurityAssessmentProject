-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2021 at 10:18 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_qa`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id1` int(11) NOT NULL,
  `rid1` var/char(11) NOT NULL,
  `text1` text NOT NULL,
  `qid` int(11) NOT NULL,
  `uid` int(9) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id1`, `rid1`, `text1`, `qid`, `uid`, `create_date`) VALUES
(1, 'بله', '؟', 185, 3, '2021-06-29 11:41:19'),
(2, 'بله', '؟', 186, 3, '2021-06-29 11:41:58'),
(3, 'خیر', ' نهاد مرجعی در نظر گرفته نشده است.', 184, 3, '2021-06-29 11:43:05'),
(4, 'بله', 'در نظر گرفته‌شده است', 148, 3, '2021-06-29 12:19:21'),
(5, 'خیر', 'در نظر گرفته‌ نشده است', 149, 3, '2021-06-29 12:20:08'),
(6, 'بله', 'در نظر گرفته‌شده است', 150, 3, '2021-06-29 12:20:36'),
(7, 'بله', 'در نظر گرفته‌شده است', 151, 3, '2021-06-29 12:21:02'),
(8, 'بله', 'در نظر گرفته‌شده است', 152, 3, '2021-06-29 12:21:20'),
(9, 'بله', 'در نظر گرفته‌شده است', 153, 3, '2021-06-29 12:22:16'),
(10, 'بله', 'در نظر گرفته‌شده است', 124, 3, '2021-06-29 12:22:35'),
(11, 'بله', 'در نظر گرفته‌شده است', 125, 3, '2021-06-29 12:23:02'),
(12, 'بله', 'در نظر گرفته‌شده است', 126, 3, '2021-06-29 12:23:20'),
(13, 'بله', 'در نظر گرفته‌شده است', 127, 3, '2021-06-29 12:23:44'),
(14, 'بله', 'در نظر گرفته‌شده است', 128, 3, '2021-06-29 12:24:06'),
(15, 'بله', 'در نظر گرفته‌شده است', 129, 3, '2021-06-29 12:24:22'),
(16, 'بله', 'در نظر گرفته‌شده است', 130, 3, '2021-06-29 12:24:45'),
(17, 'بله', 'در نظر گرفته‌شده است', 131, 3, '2021-06-29 12:25:04'),
(18, 'بله', 'در نظر گرفته‌شده است', 132, 3, '2021-06-29 12:25:19'),
(19, 'بله', 'در نظر گرفته‌شده است', 133, 3, '2021-06-29 12:25:34'),
(21, 'بله', 'در نظر گرفته‌شده است', 135, 3, '2021-06-29 12:26:16'),
(22, 'بله', 'در نظر گرفته‌شده است', 122, 3, '2021-06-29 12:28:29'),
(23, 'بله', 'در نظر گرفته‌شده است', 136, 3, '2021-06-29 12:28:52'),
(24, 'بله', 'در نظر گرفته‌شده است', 137, 3, '2021-06-29 12:29:11'),
(25, 'بله', 'در نظر گرفته‌شده است', 138, 3, '2021-06-29 12:29:31'),
(26, 'بله', 'در نظر گرفته‌شده است', 139, 3, '2021-06-29 12:29:55'),
(27, 'بله', 'در نظر گرفته‌شده است', 140, 3, '2021-06-29 12:30:11'),
(28, 'خیر', 'در نظر گرفته‌شده است', 141, 3, '2021-06-29 12:30:29'),
(29, 'بله', 'در نظر گرفته‌شده است', 142, 3, '2021-06-29 12:30:44'),
(30, 'بله', 'در نظر گرفته‌شده است', 143, 3, '2021-06-29 12:31:14'),
(31, 'بله', 'در نظر گرفته‌شده است', 144, 3, '2021-06-29 12:31:30'),
(32, 'خیر', 'دارای ضعف امنیتی است', 177, 3, '2021-06-29 12:42:22'),
(33, 'بله', 'دارای ضعف امنیتی است', 179, 3, '2021-06-29 12:43:13'),
(34, 'بله', 'دارای ضعف امنیتی است', 179, 3, '2021-06-29 12:43:27'),
(35, 'بله', 'استفاده‌شده است', 103, 3, '2021-06-29 12:44:25'),
(36, 'بله', 'بررسی‌شده‌اند', 154, 3, '2021-06-29 12:44:59'),
(37, 'بله', 'مقاوم شده‌اند', 155, 3, '2021-06-29 12:45:25'),
(38, 'بله', 'مقاوم شده‌اند', 181, 3, '2021-06-29 12:46:17'),
(39, 'بله', 'در نظر گرفته‌شده است', 182, 3, '2021-06-29 12:46:37'),
(40, 'بله', 'در نظر گرفته‌شده است', 183, 3, '2021-06-29 12:47:00'),
(41, 'بله', 'مکانیسم امن است', 114, 3, '2021-06-29 12:48:04'),
(42, 'بله', 'در نظر گرفته‌شده است', 115, 3, '2021-06-29 12:48:24'),
(43, 'بله', 'در نظر گرفته‌شده است', 107, 3, '2021-06-29 12:48:41'),
(44, 'بله', 'در نظر گرفته‌شده است', 108, 3, '2021-06-29 12:48:55'),
(45, 'بله', 'دارای حفره ی امنیتی است.', 134, 3, '2021-07-17 11:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(9) NOT NULL,
  `name_attack` varchar(256) NOT NULL,
  `Overlap_id` int(9) NOT NULL,
  `cat_id` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--


-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `view` int(9) NOT NULL,
  `status` enum('pending','publish','answered') NOT NULL DEFAULT 'pending',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--



--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(9) NOT NULL,
  `allow_login_user` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `allow_login_user`, `username`, `password`, `email`) VALUES
(2, 1, 'admin', 'e4d58a317f4e625124754e89ee8297897d2f125a', 'admin@gmail.com'),
(3, 1, 'mohammad', '4b651d88a7cadfe00e96f3604acc7e88796d041c', 'm.datkhah@gmail.com'),
(4, 0, 'AliAsgar', '4b651d88a7cadfe00e96f3604acc7e88796d041c', 'aliasgar@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id1`),
  ADD KEY `qid` (`qid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id-cat` (`cat_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`qid`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `questions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
