<?php include_once 'assets/inc/actions.php'; ?>
<?php include_once 'assets/inc/functions.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Inner Page - eNno Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

    <!--  codes project base-->
    <link rel="stylesheet" href="assets/css/pure.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
    <!-- end codes project base-->
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: eNno - v4.0.1
  * Template URL: https://bootstrapmade.com/enno-free-simple-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">


      <nav id="navbar" class="navbar font-type">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">خانه</a></li>
          <li><a class="nav-link scrollto " href="#about1">درباره چک لیست ها</a></li>
          <li class="dropdown"><a href="#about1"><span>چک لیست ها</span> <i class="bi bi-chevron-down"></i></a>
            <!--<ul>
              <li><a href="http://localhost/OLD/project%20final/7learn2/network-page.php">چک لیست های بخش شبکه</a></li>
              </li>
              <li><a href="http://localhost/OLD/project%20final/7learn2/software-page.php">چک لیست های بخش نرم افزار</a></li>
              <li><a href="http://localhost/OLD/project%20final/7learn2/hardware-page.php"> چک لیست های بخش سخت افزار</a></li>
            </ul>-->
          </li>
          <li><a class="nav-link scrollto " href="#featured-services1">ارزیابی فنی چک لیست ها</a></li>
          <li><a class="nav-link scrollto" href="#team">درباره ما</a></li>
           <?php if (isAdmin()): ?>

               <li><a class="logout" href="<?php echo QA_HOME_URL . '?logout=1'; ?>">خروج</a></li>
               <li class="dropdown"><a href="#about1"><span>ارزیاب های در انتظار تایید..</span> <i class="bi bi-chevron-down"></i></a>
                  <ul style="width: 281%">

                       <?php
                       $allow_login_user='0';
                       $users=allowLoginUser($allow_login_user);
                  foreach ($users as $user): ?>
                      <li>
                          <div class="pure-g" style="border-style: outset">
                              <div class="pure-u-1-3" style="border-style: outset;text-align: center">
                                  <form method="post"><input name="userLoginAllow" type="hidden"
                                                             value="<?php echo $user['id']; ?>"><input type="submit"
                                                                                                       value="تایید"
                                                                                                       style="background: #0dcaf0">
                                  </form>
                              </div>
                              <div class="pure-u-1-3" style="border-style: outset;text-align: center"><?php echo $user['email']; ?></div>
                              <div class="pure-u-1-3" style="border-style: outset;text-align: center"> <?php echo $user['username']; ?></div>
                          </div>
                      </li>
                        <?php endforeach; ?>
                   </ul>
               </li>
              <li> <div class="info">
                   <span class="adminName">خوش آمدی <?php echo QA_ADMIN_DISPLAYNAME; ?> عزیز</span>
               </div></li>


           <?php endif; ?>

            <?php if (isUser()): ?>
          <li> <a class="logout" href="<?php echo QA_HOME_URL . '?logout=1'; ?>">خروج</a></li>
                <div class="info">
                    <span class="adminName">خوش آمدی <?php echo $_SESSION['username']; ?> عزیز</span>
                </div>
            <?php endif; ?>


           <!--<div class="info">
            <span class="adminName">خوش آمدی <?php /*echo QA_ADMIN_DISPLAYNAME; */?> عزیز</span>
          </div> -->

          <?php if (!(isAdmin() or isUser())) : ?>
          <li>
              <i class="login" onclick="document.getElementById('id01').style.display='block'" style="width:auto;font-family:Nazanin; "> ثبت نام</i>
              / <i class="login" onclick="document.getElementById('id02').style.display='block'" style="width:auto;">ورود </i>
          </li>
           <?php endif; ?>

          <!--          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>-->
          <!--          <li><a class="getstarted scrollto" href="#about">Get Started</a></li>-->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
      <h1 class="logo"><a href="index.html"><img src="assets/img/download.png"></a></h1>
    </div>

  </header><!-- End Header -->
  <!-- ======= Hero Section ======= -->

  <section id="hero" class="d-flex align-items-center">

      <div class="container">
          <div class="row ">
              <div class=" col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center ">

                  <h1 class="font-type" ><?php echo QA_TITLE; ?></h1>
                  <h2 class="font-type"><?php echo QA_TITLE_DESCRIPTION; ?></h2>
                  <div class="d-flex font-type">
                      <a href="#about" class="btn-get-started scrollto ">شروع به کار</a>
                  </div>
              </div>
              <div class="col-lg-6 order-1 order-lg-2 hero-img">
                  <img src="assets/img/blockchain-technology.jpg" class="img-fluid animated" alt="">
              </div>
          </div>
      </div>

  </section><!-- End Hero -->
  <section>
  <?php
  if ($errorMsg1) {
      echo "<div class='error'>" . nl2br($errorMsg1) . "<br>  <a href=\"#\" onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">دوباره سعی کنید!</a></div>";
  }
  elseif ($successMsg1) {
      echo "<div class='success'>" . nl2br($successMsg1) . "</div>";
  }
  ?><?php
  if ($errorMsg) {
      echo "<div class='error'>" . nl2br($errorMsg) . "<br>  <a href=\"#\" onclick=\"document.getElementById('id02').style.display='block'\" style=\"width:auto;\">دوباره سعی کنید!</a></div>";
  }
  elseif ($successMsg) {
      echo "<div class='success'>" . nl2br($successMsg) . "</div>";
  }
  ?>

  </section>
  <!-- ======= Featured Services Section ======= -->

  <section id="featured-services" class="featured-services">
      <div class="container">

          <div class="row">
              <div class="col-lg-4 col-md-6">
                  <div class="icon-box">
                      <div class="icon"><i class="bi bi-laptop"></i></div>
                      <h4 class="title font-type"><a href="">سامانه</a></h4>
                      <p class="description font-type">این سامانه به عنوان یک دستیار جهت ارزیابی امنیتی زنجیره بلوکی(اتریوم) طراحی
                          شده است.</p>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 mt-4 mt-md-0">
                  <div class="icon-box">
                      <div class="icon"><i class="bi bi-card-checklist"></i></div>
                      <h4 class="title font-type"><a href="">ساختار چک لیست ها</a></h4>
                      <p class="description font-type">این ساختار مطابق با ساختار بیان شده در کتاب پروتکل کنترل صنعتی
                          <abdi>IEC60870-5-104</abdi>
                          از منظر امنیت سایبری است.
                      </p>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 mt-4 mt-lg-0">
                  <div class="icon-box">
                      <div class="icon"><i class="bi bi-clipboard-data"></i></div>
                      <h4 class="title font-type"><a href="">ارزیابی چک لیست</a></h4>
                      <p class="description font-type">چک لیست ها براساس پاسخ های داده شده ارزیابی می شوند.</p>
                  </div>
              </div>
          </div>

      </div>
  </section><!-- End Featured Services Section -->
  <!-- ======= Counts Section ======= -->
  <section id="counts" class="counts" >
      <div class="container">

          <div class="row counters">

              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork(null);?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد کل چک لیست ها</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php $numQuestions_hardware=getNumberAllNetwork('(لایه های شبکه)') + getNumberAllNetwork('(کیف پول سرد)')
                              + getNumberAllNetwork('(کیف پول گرم)') + getNumberAllNetwork('(الگوریتم های اجماع)');
                          echo $numQuestions_hardware;?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های بخش سخت افزار</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php $numQuestions_software=getNumberAllNetwork('(حمله فینی)') + getNumberAllNetwork('(حمله رقابتی)')
                              + getNumberAllNetwork('(حمله تاریخ جایگزین)') + getNumberAllNetwork('(حمله 51 درصد)')
                              + getNumberAllNetwork('(حمله شکل پذیری تراکنش)') + getNumberAllNetwork('(حمله خسوف)')
                              + getNumberAllNetwork('(آسیب پذیری در کد منبع قرارداد)') + getNumberAllNetwork('(آسیب پذیری در ماشین مجازی)')
                              + getNumberAllNetwork('(استخراج خودخواهانه)') + getNumberAllNetwork('(فورک پس از مضایقه)');

                          echo $numQuestions_software;?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های بخش نرم افزار</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <link rel="stylesheet" href="assets/css/w3.css">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php   $numQuestions_network=getNumberAllNetwork('(امضاهای آسیب پذیر)') + getNumberAllNetwork("(ایجاد کلید ناقص)")
                              + getNumberAllNetwork("(حمله منع سرویس توزیع شده)")+ getNumberAllNetwork("(حمله زمان ربایی)")
                              +getNumberAllNetwork("(حمله مسیریابی)") +getNumberAllNetwork("(حمله سیبیل)")
                              +getNumberAllNetwork("(حمله فیشینگ)") +getNumberAllNetwork("(حمله لغت نامه)")
                              +getNumberAllNetwork("(حمله باز پخش)");

                          echo $numQuestions_network;?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های بخش شبکه</p>
              </div>



          </div>

      </div>
  </section><!-- End Counts Section -->


 <!-- "input and register"-->
  <style>

      /* Full-width input fields */
      input[type=text], input[type=password] {
          width: 100%;
          padding: 15px;
          margin: 5px 0 22px 0;
          display: inline-block;
          border: none;
          background: #f1f1f1;
      }

      /* Add a background color when the inputs get focus */
      input[type=text]:focus, input[type=password]:focus {
          background-color: #ddd;
          outline: none;
      }

      /* Set a style for all buttons */
      button {
          background-color: #04AA6D;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
          opacity: 0.9;
      }

      button:hover {
          opacity:1;
      }

      /* Extra styles for the cancel button */
      .cancelbtn {
          padding: 14px 20px;
          background-color: #f44336;
      }

      /* Float cancel and signup buttons and add an equal width */
      .cancelbtn, .signupbtn {
          float: left;
          width: 50%;
      }

      /* Add padding to container elements */
      .container {
          padding: 16px;
      }

      /* The Modal (background) */
      .modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 1; /* Sit on top */
          left: 0;
          top: 120px;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: #474e5d;
          padding-top: 50px;
      }

      /* Modal Content/Box */
      .modal-content {
          background-color: #fefefe;
          margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
          border: 1px solid #888;
          width: 50%; /* Could be more or less, depending on screen size */
      }

      /* Style the horizontal ruler */
      hr {
          border: 1px solid #f1f1f1;
          margin-bottom: 25px;
      }

      /* The Close Button (x) */
      .close {
          position: absolute;
          right: 5px;
          top: 5px;
          font-size: 40px;
          font-weight: bold;
          color: #f3eeee;
      }

      .close:hover,
      .close:focus {
          color: #f44336;
          cursor: pointer;
      }



      /* Change styles for cancel button and signup button on extra small screens */
      @media screen and (max-width:300px) {
          .cancelbtn, .signupbtn {
              width: 100%;
          }
      }
      /* Set a grey background color and center the text of the "sign in" section */
      .signin {
          background-color: #f1f1f1;
          text-align: center;
      }
  </style>
  <div id="id01" class="modal">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      <form class="modal-content" action="" method="post" >
          <div class="container">

              <h1>فرم ثبت نام</h1>
              <p>لطفا این فرم را برای ایجاد یک حساب کاربری پر کنید.</p>
              <hr>
              <label for="psw-repeat"><b>نام کاربری</b></label>
              <input type="text" placeholder="نام کاربری خود را وارد کنید" name="username1" required>

              <label for="psw"><b>کلمه عبور</b></label>
              <input type="password" placeholder="کلمه عبور خود را وارد کنید" name="password1" required>

              <label for="email"><b>ایمیل</b></label>
              <input style="box-sizing:border-box" type="text" placeholder="ایمیل خود را وارد کنید" name="email" required>


              <div>
                  <button type="submit" name="submitUsers" >ثبت نام</button>
              </div>
          </div>
          <div class="container signin">
              <p>در حال حاضر یک حساب کاربری دارید؟ <a href="#" onclick="document.getElementById('id02').style.display='block'" style="width:auto;">ورود</a>.</p>
          </div>
      </form>
  </div>

  <script>
      // Get the modal
      var modal = document.getElementById('id01');

      // When the user clicks anywhere outside of the modal, close it
     window.onclick = function(event) {
          if (event.target == modal) {
              modal.style.display = "none";
          }
      }

  </script>
                   <!--input-->
  <div id="id02" class="modal">
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
      <form  class="modal-content" action="" method="post">
          <div class="container">


              <h1>ورود به سامانه</h1>
              <hr>
              <label for="email"><b>نام کاربری</b></label>
              <input style="box-sizing:border-box" type="text" placeholder="نام کاربری خود را وارد کنید." name="username2" required>

              <label for="psw"><b>کلمه عبور</b></label>
              <input type="password" placeholder="کلمه عبور خود را وارد کنید." name="password2" required>
              <div>
                  <button type="submit" name="submitUsername" >ورود</button>
              </div>

          </div>
      </form>
  </div>

  <script>
      // Get the modal
      var modal = document.getElementById('id02');

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == modal) {
              modal.style.display = "none";
          }
      }
  </script>

  <!-- "end input and register"-->



      <!-- ======= Breadcrumbs ======= -->

      <div class="container ">
          <section id="about1">
              <style type="text/css">
                  /*body { background-color: #f7f7f7; font-family: 'Roboto, B Nazanin'; }*/
                  /*.container { margin: 150px auto; max-width: 960px; }*/
                  .window {
                      font-weight: bold;
                      cursor: pointer;
                      border: 1px solid #346789;
                      box-shadow: 2px 2px 10px #aaa;
                      -o-box-shadow: 2px 2px 10px #aaa;
                      -webkit-box-shadow: 2px 2px 10px #aaa;
                      -moz-box-shadow: 2px 2px 10px #aaa;
                      -moz-border-radius: 0.5em;
                      border-radius: 0.5em;
                      /*
                      opacity:0.8;
                      filter:alpha(opacity=80);
                      */
                      width: 10em;
                      height: auto;
                      padding: 0.5em 0em;
                      text-align: center;
                      z-index: 20;
                      position: absolute;
                      background-color: #eeeeef;
                      color: black;
                      font-family: helvetica;
                      font-size: 0.9em;
                      word-wrap: break-word;
                  }

                  .window:hover {
                      box-shadow: 2px 2px 10px #444;
                      -o-box-shadow: 2px 2px 10px #444;
                      -webkit-box-shadow: 2px 2px 10px #444;
                      -moz-box-shadow: 2px 2px 10px #444;
                      /*
                      opacity:0.6;
                      filter:alpha(opacity=60);
                      */
                  }

                  /*
                  .window > div {
                      margin-top: 19%;
                      margin-bottom: 19%;
                  }
                  */

                  .hidden {
                      display: none;
                  }

                  .collapser {
                      cursor: pointer;
                      border: 1px dotted gray;
                      z-index: 21;
                  }

                  .errorWindow {
                      border: 2px solid red;
                  }

                  #treemain {
                      height: 1200px;
                      width: 100%;
                      position: relative;
                      overflow: auto;
                  }

              </style>
              <div id="jquery-script-menu">
                  <div class="jquery-script-center">
                      <div id="carbon-block"></div>
                      <div class="jquery-script-ads">
                          <script type="text/javascript"><!--
                              google_ad_client = "ca-pub-2783044520727903";
                              /* jQuery_demo */
                              google_ad_slot = "2780937993";
                              google_ad_width = 728;
                              google_ad_height = 90;
                              //-->
                          </script>
                      </div>
                  </div>
              </div>
              <!--hardware-->
              <?php
                   if(isUser()){
                  $numQuestions_hardware1=getNumberAllNetwork('(کیف پول سرد)');
                  $numQuestions_hardware2=getNumberAllNetwork('(کیف پول گرم)');
                  $numQuestions_hardware3=getNumberAllNetwork('(الگوریتم های اجماع)');
                       $numQuestions_hardwareAll= $numQuestions_hardware1+ $numQuestions_hardware2+ $numQuestions_hardware3 ;

                  $countPendingQuestion1=getcountStatusQuestion('(کیف پول سرد)','answered', $_SESSION['id']);
                  $countPendingQuestion1p=getcountStatusQuestion('(کیف پول سرد)','publish', $_SESSION['id']);
                  $countPendingQuestion2=getcountStatusQuestion('(کیف پول گرم)','answered', $_SESSION['id']);
                  $countPendingQuestion2p=getcountStatusQuestion('(کیف پول گرم)','publish', $_SESSION['id']);
                  $countPendingQuestion3=getcountStatusQuestion('(الگوریتم های اجماع)','answered',$_SESSION['id']);
                  $countPendingQuestion3p=getcountStatusQuestion('(الگوریتم های اجماع)','publish',$_SESSION['id']);

                       $countPendingQuestionAll=$countPendingQuestion1+$countPendingQuestion2+$countPendingQuestion3;
                       $countPendingQuestionpAll1=$countPendingQuestion1p+$countPendingQuestion2p+$countPendingQuestion3p;

              //<!--software-->

              $numQuestions_software1 = getNumberAllNetwork('(حمله فینی)');
              $numQuestions_software2 = getNumberAllNetwork('(حمله رقابتی)');
              $numQuestions_software3 = getNumberAllNetwork('(حمله تاریخ جایگزین)');
              $numQuestions_software4 = getNumberAllNetwork('(حمله 51 درصد)');
              $numQuestions_software5 = getNumberAllNetwork('(حمله شکل پذیری تراکنش)');
              $numQuestions_software6 = getNumberAllNetwork('(حمله خسوف)');
              $numQuestions_software7 = getNumberAllNetwork('(آسیب پذیری در کد منبع قرارداد)');
              $numQuestions_software8 = getNumberAllNetwork('(آسیب پذیری در ماشین مجازی)');
              $numQuestions_software9 = getNumberAllNetwork('(استخراج خودخواهانه)');
              $numQuestions_software10 = getNumberAllNetwork('(فورک پس از مضایقه)');

                       $numQuestions_software1to6 = $numQuestions_software1 + $numQuestions_software2 + $numQuestions_software3
                           + $numQuestions_software4 + $numQuestions_software5 + $numQuestions_software6;
                       $numQuestions_software7to8 = $numQuestions_software7 + $numQuestions_software8;
                       $numQuestions_software9to10 = $numQuestions_software9 + $numQuestions_software10;

                       $numQuestions_softwareAll = $numQuestions_software1to6 + $numQuestions_software7to8 + $numQuestions_software9to10;

              $countPublishsQuestion1 = getcountStatusQuestion('(حمله فینی)', 'answered', $_SESSION['id']);
              $countPublishsQuestion1p = getcountStatusQuestion('(حمله فینی)', 'publish', $_SESSION['id']);
              $countPublishsQuestion2 =getcountStatusQuestion('(حمله رقابتی)', 'answered', $_SESSION['id']);
              $countPublishsQuestion2p =getcountStatusQuestion('(حمله رقابتی)', 'publish', $_SESSION['id']);
              $countPublishsQuestion3 =getcountStatusQuestion('(حمله تاریخ جایگزین)', 'answered', $_SESSION['id']);
              $countPublishsQuestion3p =getcountStatusQuestion('(حمله تاریخ جایگزین)', 'publish', $_SESSION['id']);
              $countPublishsQuestion4 =getcountStatusQuestion('(حمله 51 درصد)', 'answered', $_SESSION['id']);
              $countPublishsQuestion4p =getcountStatusQuestion('(حمله 51 درصد)', 'publish', $_SESSION['id']);
              $countPublishsQuestion5 =getcountStatusQuestion('(حمله شکل پذیری تراکنش)', 'answered', $_SESSION['id']);
              $countPublishsQuestion5p =getcountStatusQuestion('(حمله شکل پذیری تراکنش)', 'publish', $_SESSION['id']);
              $countPublishsQuestion6 =getcountStatusQuestion('(حمله خسوف)', 'answered', $_SESSION['id']);
              $countPublishsQuestion6p =getcountStatusQuestion('(حمله خسوف)', 'publish', $_SESSION['id']);
              $countPublishsQuestion7 =getcountStatusQuestion('(آسیب پذیری در کد منبع قرارداد)', 'answered', $_SESSION['id']);
              $countPublishsQuestion7p =getcountStatusQuestion('(آسیب پذیری در کد منبع قرارداد)', 'publish', $_SESSION['id']);
              $countPublishsQuestion8 =getcountStatusQuestion('(آسیب پذیری در ماشین مجازی)', 'answered', $_SESSION['id']);
              $countPublishsQuestion8p =getcountStatusQuestion('(آسیب پذیری در ماشین مجازی)', 'publish', $_SESSION['id']);
              $countPublishsQuestion9 =getcountStatusQuestion('(استخراج خودخواهانه)', 'answered', $_SESSION['id']);
              $countPublishsQuestion9p =getcountStatusQuestion('(استخراج خودخواهانه)', 'publish', $_SESSION['id']);
              $countPublishsQuestion10 =getcountStatusQuestion('(فورک پس از مضایقه)', 'answered', $_SESSION['id']);
              $countPublishsQuestion10p =getcountStatusQuestion('(فورک پس از مضایقه)', 'publish', $_SESSION['id']);

                       $countPublishsQuestion1to6=$countPublishsQuestion1+$countPublishsQuestion2+$countPublishsQuestion3
                                                  +$countPublishsQuestion4+$countPublishsQuestion5+$countPublishsQuestion6;
                       $countPublishsQuestion7to8=$countPublishsQuestion7+$countPublishsQuestion8;
                       $countPublishsQuestion9to10=$countPublishsQuestion9+$countPublishsQuestion10;
                       $countPublishsQuestionAll=$countPublishsQuestion1to6+ $countPublishsQuestion7to8+$countPublishsQuestion9to10 ;
                      /*--------------------------------------*/
                       $countPublishsQuestionp1to6=$countPublishsQuestion1p+$countPublishsQuestion2p+$countPublishsQuestion3p
                           +$countPublishsQuestion4p+$countPublishsQuestion5p+$countPublishsQuestion6p;
                       $countPublishsQuestionp7to8=$countPublishsQuestion7p+$countPublishsQuestion8p;
                       $countPublishsQuestionp9to10=$countPublishsQuestion9p+$countPublishsQuestion10p;
                       $countPublishsQuestionpAll2=$countPublishsQuestionp1to6+ $countPublishsQuestionp7to8+$countPublishsQuestionp9to10 ;


                       // <!--network-->


              $numQuestions_network1 = getNumberAllNetwork('(امضاهای آسیب پذیر)');
              $numQuestions_network2 = getNumberAllNetwork("(ایجاد کلید ناقص)");
              $numQuestions_network3 = getNumberAllNetwork("(حمله منع سرویس توزیع شده)");
              $numQuestions_network4 = getNumberAllNetwork("(حمله زمان ربایی)");
              $numQuestions_network5 = getNumberAllNetwork("(حمله مسیریابی)");
              $numQuestions_network6 = getNumberAllNetwork("(حمله سیبیل)");
              $numQuestions_network7 = getNumberAllNetwork("(حمله فیشینگ)");
              $numQuestions_network8 = getNumberAllNetwork("(حمله لغت نامه)");
              $numQuestions_network9 = getNumberAllNetwork("(حمله باز پخش)");
                       $numQuestions_networkAll = $numQuestions_network1 + $numQuestions_network2 + $numQuestions_network3 + $numQuestions_network4
                           + $numQuestions_network5 + $numQuestions_network6 + $numQuestions_network7 + $numQuestions_network8 + $numQuestions_network9;

              $countPublishQuestion1 = getcountStatusQuestion('(امضاهای آسیب پذیر)', 'answered', $_SESSION['id']);
              $countPublishQuestion1p = getcountStatusQuestion('(امضاهای آسیب پذیر)', 'publish', $_SESSION['id']);
              $countPublishQuestion2 = getcountStatusQuestion("(ایجاد کلید ناقص)", 'answered', $_SESSION['id']);
              $countPublishQuestion2p = getcountStatusQuestion("(ایجاد کلید ناقص)", 'publish', $_SESSION['id']);
              $countPublishQuestion3 = getcountStatusQuestion("(حمله منع سرویس توزیع شده)", 'answered', $_SESSION['id']);
              $countPublishQuestion3p = getcountStatusQuestion("(حمله منع سرویس توزیع شده)", 'publish', $_SESSION['id']);
              $countPublishQuestion4 = getcountStatusQuestion("(حمله زمان ربایی)", 'answered', $_SESSION['id']);
              $countPublishQuestion4p = getcountStatusQuestion("(حمله زمان ربایی)", 'publish', $_SESSION['id']);
              $countPublishQuestion5 = getcountStatusQuestion("(حمله مسیریابی)", 'answered', $_SESSION['id']);
              $countPublishQuestion5p = getcountStatusQuestion("(حمله مسیریابی)", 'publish', $_SESSION['id']);
              $countPublishQuestion6 = getcountStatusQuestion("(حمله سیبیل)", 'answered', $_SESSION['id']);
              $countPublishQuestion6p = getcountStatusQuestion("(حمله سیبیل)", 'publish', $_SESSION['id']);
              $countPublishQuestion7 = getcountStatusQuestion("(حمله فیشینگ)", 'answered', $_SESSION['id']);
              $countPublishQuestion7p = getcountStatusQuestion("(حمله فیشینگ)", 'publish', $_SESSION['id']);
              $countPublishQuestion8 = getcountStatusQuestion("(حمله لغت نامه)", 'answered', $_SESSION['id']);
              $countPublishQuestion8p = getcountStatusQuestion("(حمله لغت نامه)", 'publish', $_SESSION['id']);
              $countPublishQuestion9 = getcountStatusQuestion("(حمله باز پخش)", 'answered', $_SESSION['id']);
              $countPublishQuestion9p = getcountStatusQuestion("(حمله باز پخش)", 'publish', $_SESSION['id']);

                       $countPublishQuestionAll=$countPublishQuestion1+$countPublishQuestion2+$countPublishQuestion3
                                               +$countPublishQuestion4 +$countPublishQuestion5+$countPublishQuestion6
                                               +$countPublishQuestion7+$countPublishQuestion8+ $countPublishQuestion9;

                       $countPublishQuestionpAll3=$countPublishQuestion1p+$countPublishQuestion2p+$countPublishQuestion3p
                                                +$countPublishQuestion4p+$countPublishQuestion5p+$countPublishQuestion6p
                                                +$countPublishQuestion7p+$countPublishQuestion8p+$countPublishQuestion9p;

                       $countPublishQuestionpAllAll=$countPublishQuestionpAll3+$countPublishsQuestionpAll2+$countPendingQuestionpAll1;
                   }
              ?>

              <div class="container">
                  <h1>برخی از حملات و راه های نفوذ به شبکه زنجیره بلوکی (اتریوم)</h1>
                  <p>در این گزارش تمام راه های حمله و نفوذ به شبکه زنجیره بلوکی (اتریوم) و مرتبط با آن را در قالب نمودار
                      درختی
                      زیر دسته بندی کرده ایم.</p>
                  </p>
                  <div id="treemain">

                      <div id="node_0" class="window hidden"
                           data-id="0"
                           data-parent=""
                           data-first-child="1"
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                              <a href="<?php echo QA_HOME_URL . 'hardware-page.php#breadcrumbs'; ?>">
                                  <?php if ( $numQuestions_hardwareAll==$countPendingQuestionAll):?>
                                      <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg>
                          </span>
                                  <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestionpAllAll;?></span><?php endif;?>
                                  حملات و راه های نفوذ به زنجیره بلوکی</a>
                          <?php endif; ?>
                          <?php if ( isAdmin()): ?>
                              حملات و راه های نفوذ به زنجیره بلوکی
                          <?php endif; ?>
                      </div>

                      <div id="node_1" class="window hidden"
                           data-id="1"
                           data-parent="0"
                           data-first-child="2"
                           data-next-sibling="6">
                          <?php if (isAdmin()): ?>
                              <a href="<?php echo QA_HOME_URL . 'hardware-page.php#breadcrumbs'; ?>"> امنیت سخت
                                  افزار</a>
                          <?php endif; ?>

                          <?php if (isUser()): ?>
                              <a href="<?php echo QA_HOME_URL . 'hardware-page.php#breadcrumbs'; ?>">
                                      <?php if ( $numQuestions_hardwareAll==$countPendingQuestionAll):?>
                                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg>
                          </span>
                                      <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPendingQuestionpAll1;?></span><?php endif;?>
                                  امنیت سخت
                                  افزار</a>
                          <?php endif; ?>

                          <?php if (!(isUser() or isAdmin())): ?>
                              امنیت سخت افزار
                          <?php endif; ?>
                      </div>

                      <div id="node_2" class="window hidden"
                           data-id="2"
                           data-parent="1"
                           data-first-child=""
                           data-next-sibling="3">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_hardware1==$countPendingQuestion1):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg>
                          </span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPendingQuestion1p;?></span><?php endif;?>
                          <?php endif;?> حمله به کیف پول
                          سرد
                          </a>

                      </div>

                      <div id="node_3" class="window hidden"
                           data-id="3"
                           data-parent="1"
                           data-first-child=""
                           data-next-sibling="4">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_hardware2==$countPendingQuestion2):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg>
                          </span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPendingQuestion2p;?></span><?php endif;?>
                          <?php endif;?>
                          حمله به کیف پول
                          گرم
                          </a>
                      </div>

                      <div id="node_4" class="window hidden"
                           data-id="4"
                           data-parent="1"
                           data-first-child=""
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_hardware3==$countPendingQuestion3):?>
                          <span style="background: #16df7e">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                               class="bi1 bi-check2-circle" viewBox="0 0 16 16"  style="background:#0dcaf0">
                              <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                              <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                          </svg></span>
                          <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPendingQuestion3p;?></span><?php endif;?>
                          <?php endif;?>
                          الگوریتم های
                          اجماع
                          </a>

                      </div>


                      <!--========== پایان بخش سخت افزار============-->
                      <div id="node_6" class="window hidden"
                           data-id="6"
                           data-parent="0"
                           data-first-child="7"
                           data-next-sibling="20">

                          <?php if (isAdmin()): ?>
                              <a href="<?php echo QA_HOME_URL . 'software-page.php#breadcrumbs'; ?>"> امنیت نرم
                                  افزار</a> <?php endif; ?>

                          <?php if (isUser()): ?>
                              <a href="<?php echo QA_HOME_URL . 'software-page.php#breadcrumbs'; ?>">
                                  <?php if ( $numQuestions_softwareAll== $countPublishsQuestionAll):?>
                                      <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                                  <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestionpAll2;?></span><?php endif;?>
                                  امنیت نرم
                                  افزار</a> <?php endif; ?>

                          <?php if (!(isUser() or isAdmin())): ?>
                              امنیت نرم افزار
                          <?php endif; ?>
                      </div>
                      <div id="node_7" class="window hidden"
                           data-id="7"
                           data-parent="6"
                           data-first-child="8"
                           data-next-sibling="14"> <?php if (isUser()): ?>
                              <?php if ( $numQuestions_software1to6==$countPublishsQuestion1to6):?>
                                  <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestionp1to6;?></span><?php endif;?>
                          <?php endif;?>
                          امنیت تراکنش ها
                      </div>

                      <div id="node_8" class="window hidden"
                           data-id="8"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="9">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software1==$countPublishsQuestion1):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion1p;?></span><?php endif;?>
                          <?php endif;?>حمله فینی</a>

                      </div>

                      <div id="node_9" class="window hidden"
                           data-id="9"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="10">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software2==$countPublishsQuestion2):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion2p;?></span><?php endif;?>
                          <?php endif;?>حمله رقابتی</a>

                      </div>

                      <div id="node_10" class="window hidden"
                           data-id="10"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="11">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software3==$countPublishsQuestion3):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion3p;?></span><?php endif;?>
                          <?php endif;?>حمله تاریخ جایگزین</a>

                      </div>

                      <div id="node_11" class="window hidden"
                           data-id="11"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="12">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software4==$countPublishsQuestion4):?>
                          <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion4p;?></span><?php endif;?>
                          <?php endif;?>حمله 51 درصد</a>

                      </div>

                      <div id="node_12" class="window hidden"
                           data-id="12"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="13">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software5==$countPublishsQuestion5):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion5p;?></span><?php endif;?>
                          <?php endif;?>حمله شکل پذیری
                          تراکنش</a>

                      </div>

                      <div id="node_13" class="window hidden"
                           data-id="13"
                           data-parent="7"
                           data-first-child=""
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software6==$countPublishsQuestion6):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion6p;?></span><?php endif;?>
                          <?php endif;?> کشف حمله خسوف</a>

                      </div>
                      <!--===========پایان عملیات تراکنش ها=================-->
                      <div id="node_14" class="window hidden"
                           data-id="14"
                           data-parent="6"
                           data-first-child="15"
                           data-next-sibling="17"> <?php if (isUser()): ?>
                              <?php if ( $numQuestions_software7to8==$countPublishsQuestion7to8):?>
                                  <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestionp7to8;?></span><?php endif;?>
                          <?php endif;?>
                          حمله های قرار داد هوشمند
                      </div>

                      <div id="node_15" class="window hidden"
                           data-id="15"
                           data-parent="14"
                           data-first-child=""
                           data-next-sibling="16">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software7==$countPublishsQuestion7):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion7p;?></span><?php endif;?>
                          <?php endif;?>آسیب پذیری در کد منبع
                          قرارداد</a>

                      </div>

                      <div id="node_16" class="window hidden"
                           data-id="16"
                           data-parent="14"
                           data-first-child=""
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software8==$countPublishsQuestion8):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion8p;?></span><?php endif;?>
                          <?php endif;?> آسیب پذیری در ماشین
                          مجازی</a>

                      </div>

                      <div id="node_17" class="window hidden"
                           data-id="17"
                           data-parent="6"
                           data-first-child="18"
                           data-next-sibling=""> <?php if (isUser()): ?>
                              <?php if ( $numQuestions_software9to10==$countPublishsQuestion9to10):?>
                                  <span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestionp9to10;?></span><?php endif;?>
                          <?php endif;?>
                          حمله های استخرهای استخراج
                      </div>

                      <div id="node_18" class="window hidden"
                           data-id="18"
                           data-parent="17"
                           data-first-child=""
                           data-next-sibling="19">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software9==$countPublishsQuestion9):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion9p;?></span><?php endif;?>
                          <?php endif;?>استخراج خودخواهانه</a>

                      </div>

                      <div id="node_19" class="window hidden"
                           data-id="19"
                           data-parent="17"
                           data-first-child=""
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_software10==$countPublishsQuestion10):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishsQuestion10p;?></span><?php endif;?>
                          <?php endif;?>فورک پس از مضایقه</a>

                      </div>
                      <div id="node_20" class="window hidden"
                           data-id="20"
                           data-parent="0"
                           data-first-child="21"
                           data-next-sibling="">
                          <?php if (isAdmin()): ?>
                              <a href="<?php echo QA_HOME_URL . 'network-page.php#breadcrumbs'; ?>">شبکه، پروتکل های
                                  رمزنگاری و تبادل کلید </a> <?php endif; ?>
                          <?php if (isUser()): ?>
                              <a href="<?php echo QA_HOME_URL . 'network-page.php#breadcrumbs'; ?>">
                                  <?php if ( $numQuestions_networkAll==$countPublishQuestionAll):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                                  <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestionpAll3;?></span><?php endif;?>

                                  شبکه، پروتکل های
                                  رمزنگاری و تبادل کلید</a> <?php endif; ?>
                          <?php if (!(isUser() or isAdmin())): ?>
                              شبکه، پروتکل های رمزنگاری و تبادل کلید
                          <?php endif; ?></div>


                      <div id="node_21" class="window hidden"
                           data-id="21"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="22">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network1==$countPublishQuestion1):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion1p;?></span><?php endif;?>
                          <?php endif;?>امضاهای آسیب پذیر</a>

                      </div>
                      <div id="node_22" class="window hidden"
                           data-id="22"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="23">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network2==$countPublishQuestion2):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion2p;?></span><?php endif;?>
                          <?php endif;?>ایجاد کلید ناقص</a>

                      </div>
                      <div id="node_23" class="window hidden"
                           data-id="23"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="24">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network3==$countPublishQuestion3):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion3p;?></span><?php endif;?>
                          <?php endif;?>حمله منع سرویس توزیع
                          شده</a>

                      </div>
                      <div id="node_24" class="window hidden"
                           data-id="24"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="25">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network4==$countPublishQuestion4):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion4p;?></span><?php endif;?>
                          <?php endif;?>حمله زمان ربایی</a>

                      </div>
                      <div id="node_25" class="window hidden"
                           data-id="25"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="26">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network5==$countPublishQuestion5):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion5p;?></span><?php endif;?>
                          <?php endif;?>حمله مسیریابی</a>

                      </div>
                      <div id="node_26" class="window hidden"
                           data-id="26"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="27">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network6==$countPublishQuestion6):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion6p;?></span><?php endif;?>
                          <?php endif;?>حمله سیبیل</a>

                      </div>
                      <div id="node_27" class="window hidden"
                           data-id="27"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="28">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network7==$countPublishQuestion7):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion7p;?></span><?php endif;?>
                          <?php endif;?>حمله فیشینگ</a>

                      </div>
                      <div id="node_28" class="window hidden"
                           data-id="28"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="29">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network8==$countPublishQuestion8):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion8p;?></span><?php endif;?>
                          <?php endif;?>حمله لغت نامه</a>

                      </div>
                      <div id="node_29" class="window hidden"
                           data-id="29"
                           data-parent="20"
                           data-first-child=""
                           data-next-sibling="">
                          <?php if (isUser()): ?>
                          <?php if ( $numQuestions_network9==$countPublishQuestion9):?><span style="background: #16df7e">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                   class="bi bi-check2-circle" viewBox="0 0 16 16">
                                  <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                  <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                              </svg></span>
                              <?php else: ?><span style="background: #0dcaf0;color: #191818" class="badge badge-light"><?php echo $countPublishQuestion9p;?></span><?php endif;?>
                          <?php endif;?> حمله باز پخش</a>
                      </div>
                  </div>
              </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jsPlumb/1.7.10/jsPlumb.min.js"
              integrity="sha512-A1gTsaWUck7mkEu6D8/938PKlkVS79TkgqAloQbGU4bhOPUBS9JVknN5x++J3eRNO8g6D/T3kqhHBd4KkqRNxg=="
              crossorigin="anonymous"></script>
      <script type="text/javascript" src="assets/js/jsplumb-tree.js"></script>

      <script type="text/javascript">
          // -- init -- //
          jsPlumb.ready(function () {

              // connection lines style
              var connectorPaintStyle = {
                  lineWidth: 3,
                  strokeStyle: "#4F81BE",
                  joinstyle: "round"
              };

              var pdef = {
                  // disable dragging
                  DragOptions: null,
                  // the tree container
                  Container: "treemain"
              };
              var plumb = jsPlumb.getInstance(pdef);

              // all sizes are in pixels
              var opts = {
                  prefix: 'node_',
                  // left margin of the root node
                  baseLeft: 24,
                  // top margin of the root node
                  baseTop: 24,
                  // node width
                  nodeWidth: 100,
                  // horizontal margin between nodes
                  hSpace: 36,
                  // vertical margin between nodes
                  vSpace: 10,
                  imgPlus: 'tree_expand.png',
                  imgMinus: 'tree_collapse.png',
                  // queste non sono tutte in pixel
                  sourceAnchor: [1, 0.5, 1, 0, 10, 0],
                  targetAnchor: "LeftMiddle",
                  sourceEndpoint: {
                      endpoint: ["Image", {url: "tree_collapse.png"}],
                      cssClass: "collapser",
                      isSource: true,
                      connector: ["Flowchart", {
                          stub: [40, 60],
                          gap: [10, 0],
                          cornerRadius: 5,
                          alwaysRespectStubs: false
                      }],
                      connectorStyle: connectorPaintStyle,
                      enabled: false,
                      maxConnections: -1,
                      dragOptions: null
                  },
                  targetEndpoint: {
                      endpoint: "Blank",
                      maxConnections: -1,
                      dropOptions: null,
                      enabled: false,
                      isTarget: true
                  },
                  connectFunc: function (tree, node) {
                      var cid = node.data('id');
                      console.log('Connecting node ' + cid);
                  }
              };
              var tree = jQuery.jsPlumbTree(plumb, opts);
              tree.init();
              window.treemain = tree;
          });

          function positioningBlockBug() {
              var oldNode = window.treemain.nodeById(2);
              //var newNode = $('#node_2_new');
              var newNode = $('    <div id="node_2" class="window hidden"\n' +
                  '         data-id="2"\n' +
                  '         data-parent="0"\n' +
                  '         data-first-child="6"\n' +
                  '         data-next-sibling="3">\n' +
                  '        Node 2 NEW\n' +
                  '    </div>\n');
              if (oldNode) {
                  // butta il nodo nel container
                  oldNode.replaceWith(newNode);
                  // rimostra il nodo
                  newNode.id = 'node_2';
                  newNode.show();
                  // aggiorna l'albero
                  window.treemain.update();
              }

          }

      </script>
      <script>
          try {
              fetch(new Request("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", {
                  method: 'HEAD',
                  mode: 'no-cors'
              })).then(function (response) {
                  return true;
              }).catch(function (e) {
                  var carbonScript = document.createElement("script");
                  carbonScript.src = "//cdn.carbonads.com/carbon.js?serve=CK7DKKQU&placement=wwwjqueryscriptnet";
                  carbonScript.id = "_carbonads_js";
                  document.getElementById("carbon-block").appendChild(carbonScript);
              });
          } catch (error) {
              console.log(error);
          }
      </script>
      <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-36251023-1']);
          _gaq.push(['_setDomainName', 'jqueryscript.net']);
          _gaq.push(['_trackPageview']);

          (function () {
              var ga = document.createElement('script');
              ga.type = 'text/javascript';
              ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(ga, s);
          })();

      </script>
  </section>


  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
      <div class="container">

          <div class="section-title">
              <h2>گروه</h2>
          </div>

          <div class="row">
              <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                  <div class="member">
                      <img src="assets/img/unnamed.jpg" alt="">
                      <h4> دکتر رحیم اصغری</h4>
                      <span>مجری پروژه</span>
                      <p>
                          Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut
                          aut aut
                      </p>
                      <div class="social">
                          <a href=""><i class="bi bi-twitter"></i></a>
                          <a href=""><i class="bi bi-facebook"></i></a>
                          <a href=""><i class="bi bi-instagram"></i></a>
                          <a href=""><i class="bi bi-linkedin"></i></a>
                      </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                  <div class="member">
                      <img src="assets/img/70714=89-09-11 .JPG" alt="">
                      <h4>مهندس محمددادخواه</h4>
                      <span>همکار پروژه</span>
                      <p>
                          Repellat fugiat adipisci nemo illum nesciunt voluptas repellendus. In architecto rerum rerum
                          temporibus
                      </p>
                      <div class="social">
                          <a href=""><i class="bi bi-twitter"></i></a>
                          <a href=""><i class="bi bi-facebook"></i></a>
                          <a href=""><i class="bi bi-instagram"></i></a>
                          <a href=""><i class="bi bi-linkedin"></i></a>
                      </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                  <div class="member">
                      <img src="assets/img/publish.png" alt="">
                      <h4>   </h4>
                      <span>CTO</span>
                      <p>
                          Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des clara
                      </p>
                      <div class="social">
                          <a href=""><i class="bi bi-twitter"></i></a>
                          <a href=""><i class="bi bi-facebook"></i></a>
                          <a href=""><i class="bi bi-instagram"></i></a>
                          <a href=""><i class="bi bi-linkedin"></i></a>
                      </div>
                  </div>
              </div>

          </div>

      </div>
  </section><!-- End Team Section -->
  <!-- ======= Footer ======= -->
  <footer id="footer">
      <div class="container footer-bottom clearfix">
      <div class="copyright">
          &copy; کپی رایت <strong><span>دانشگاه صنعتی مالک اشتر(تهران)</span></strong> تمامی حقوق محفوظ است.
      </div>
      <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
          طراحی شده توسط: <a href="https://mail.google.com/mail/m.datkhah@gmail.com/">مهندس محمد دادخواه قلعه جوق</a>
      </div>
      </div>
  </footer>
   <!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
<!--codes project base-->
  <script src="assets/js/scripts.js"></script>
  <?php if (isAdmin()): ?>
      <script src="assets/js/admin.js"></script>
  <?php endif; ?>
  <!-- end codes project base-->

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>