<?php include_once 'assets/inc/actions.php'; ?>
<?php include_once 'assets/inc/config.php'; ?>
<!DOCTYPE html>
<html lang="fn">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

<!--  <title>Inner Page - eNno Bootstrap Template</title>-->
  <meta content="" name="description">
  <meta content="" name="keywords">
<!--    از فایل login.php-->
    <link rel="stylesheet" href="assets/css/pure.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
<!--  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">-->

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">
      <nav id="navbar" class="navbar font-type">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">خانه</a></li>
          <li><a class="nav-link scrollto " href="#about">درباره چک لیست ها</a></li>
          <li class="dropdown"><a href="#checklist"><span>چک لیست ها</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#checklist">چک لیست های بخش شبکه</a></li>
              </li>
              <li><a href="#checklist">چک لیست های بخش نرم افزار</a></li>
              <li><a href="#checklist"> چک لیست های بخش سخت افزار</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto " href="#featured-services1">ارزیابی فنی چک لیست ها</a></li>
          <li><a class="nav-link scrollto" href="#team">درباره ما</a></li>
            <li><a class="nav-link scrollto" href="#section1">ورود</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
        <!-- .navbar -->
        <h1 class="logo"><a><img src="assets/img/download.png"></a></h1>
    </div>
  </header><!-- End Header -->
  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
      <link rel="stylesheet" href="assets/css/login/login.css">
    <section id="section1" class="breadcrumbs">
      <div class="container">
          <div style="direction: rtl" class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">

                  <div class="card2 card border-0 px-4 py-5">
                      <?php
                      if ($errorMsg) {
                          echo "<div class='error'>" . nl2br($errorMsg) . "</div>";
                      } elseif ($successMsg) {
                          echo "<div class='success'>" . nl2br($successMsg) . "</div>";
                      }
                      ?>
                      <form  action=" " method="post" class="row mb-4 px-3 ltr pure-form loginform">
                          <h6 class="mb-0 mr-4 mt-2"><h1 ><a  href="http://localhost/OLD/project%20final/7learn2/"><?php echo QA_TITLE; ?></a></h1><br>
                          <h2>فرم ورود:</h2></h6>
                      <div class="row px-3 mb-4">
                          <div class="line"></div> <small class="or text-center"></small>
                          <div class="line"></div>
                      </div>
                      <div class="row px-3"> <label class="mb-1">
                              <h6 class="mb-0 text-sm">نام کاربری</h6>
                          </label> <input class="mb-4 ltr" type="text" name="username" placeholder="نام کاربری خود را وارد کنید"> </div>
                      <div class="row px-3"> <label class="mb-1">
                              <h6 class="mb-0 text-sm">کلمه عبور</h6>
                          </label> <input class="ltr" type="password" name="password" placeholder="کلمه عبور خود را وارد کنید"> </div>
                      <div class="row mb-3 px-3"> <button type="submit" name="login" value="Login" class="btn btn-blue text-center">ورود</button> </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

        <div class="d-flex justify-content-between align-items-center">

         <!-- <ol>
            <li><a href="index.php">خانه</a></li>
            <li><?php /*echo QA_TITLE; */?></li>
          </ol>
        </div>
-->
      </div>
    </section><!-- End Breadcrumbs -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
<!--    <div class="footer-top">-->
  <hr>
    <div class="container footer-bottom">
      <div class="copyright">
        &copy; کپی رایت <strong><span> دانشگاه صنعتی مالک اشتر (تهران) </span></strong>تمامی حقوق محفوظ است.
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
        طراحی شده توسط: <a href="https://mail.google.com/mail/m.datkhah@gmail.com/">مهندس محمددادخواه</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/admin.js"></script>
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>