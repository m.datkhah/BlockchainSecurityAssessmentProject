<?php
include_once 'functions.php';

if (isset($_GET['action']) ) {
    sleep(1);

        $action = $_GET['action'];
        if ($action == 'qm') { // do question management
            $qmArr = explode('-', $_POST['qmStr']);
            $operation = $qmArr[0];
            $qid = $qmArr[1];
            if ($operation == 'qmd') {
                removeQuestion($qid);
                echo 'حذف شد .';
            } elseif ($operation == 'qmpe') {
                changeQuestionStatus($qid, 'pending');
                echo 'لغو تائید انجام شد .';
            } elseif ($operation == 'qmpu') {
                changeQuestionStatus($qid, 'publish');
                echo 'تائید و منتشر شد .';
            }
        }
}
?>