<?php
session_start();
include_once 'config.php';
require_once 'jdf.php';

/***** Helper Functions *****/
function myPrint($var)
{
    echo '<pre class="ltr">';
    if (is_array($var) or is_object($var))
        print_r($var);
    else
        echo $var;
    echo '</pre>';
}


/***** Validation Functions *****/
function isValidAjaxRequest()
{
    if (1) {
        return true;
    }
    return false;
}

function isValidStatus($status)
{
    $statusArr = array('pending', 'publish', 'answered');
    if (in_array($status, $statusArr))
        return true;
    return false;
}

function getValidStatus($status)
{
    if (isValidStatus($status)) {
        return $status;
    } else {
        return 'all';
    }
}

function isValidQuestion($qText, $rid1, $qid, &$errMsg3 = null)
{
    $errMsg3 = '';
    $hasError = false;
    if (strlen($qText) < QA_ANSWERS_MIN_LENGTH) {
        $errMsg3 .= "پاسخ کوتاه است.\n";
        $hasError = true;
    }
    if (strlen($rid1) == 0) {
        $errMsg3 .= "گزینه ها اتخاب نشده است.\n";
        $hasError = true;
    }

    if (strlen($qid) == QA_UNAMBER_MIN_LENGTH) {
        $errMsg3 .= "شماره سوال وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    if (strlen($qid) > QA_UNAMBER_MAX_LENGTH) {
        $errMsg3 .= "شماره سوال وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    if ($hasError) {
        return false;
    }
    return true;
}

function isValidAdminQuestion($AdminQuestion, $rid3, $view, &$errMsg3 = null)
{
    $errMsg3 = '';
    $hasError = false;

    if (strlen($rid3) == 0) {
        $errMsg3 .= "گزینه ها اتخاب نشده است.\n";
        $hasError = true;
    }
    if (strlen($view) == 0) {
        $errMsg3 .= "گزینه ها اتخاب نشده است.\n";
        $hasError = true;
    }

    if ($hasError) {
        return false;
    }
    return true;
}

function isValidCategories($qid, $rid2, $Overlap_id, &$errMsg3 = null)
{
    print($qid);
    $errMsg3 = '';
    $hasError = false;
    if (strlen($qid) == QA_UNAMBER_MIN_LENGTH) {
        $errMsg3 .= "شماره سوال وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    if (strlen($qid) > QA_UNAMBER_MAX_LENGTH) {
        $errMsg3 .= "شماره سوال وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    if (strlen($Overlap_id) == QA_UNAMBER_MIN_LENGTH) {
        $errMsg3 .= "شماره سوال هم پوشانی وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    if (strlen($Overlap_id) > QA_UNAMBER_MAX_LENGTH) {
        $errMsg3 .= "شماره سوال هم پوشانی وارد شده نامعتبر است.\n";
        $hasError = true;
    }
    /*    if ($rid2 == 0){
        $errMsg3 .= "گزینه ای انتخاب نشده است.\n";
        $hasError = true;
    }*/
    if ($hasError) {
        return false;
    }
    return true;
}

function printVar($var)
{
    foreach ($GLOBALS as $var_name => $value) {
        if ($value === $var) {
            $vType = '<span style="color:#3792CF">' . gettype($var) . '</span>';
            $vName = '<span style="color:#b51997"> $' . $var_name . '</span> : ';
            echo '<div style="font-family: \'Courier New\';padding: 5px 0 6px 0;border-bottom: 1px dashed silver;">';
            if (is_array($var)) {
                echo $vType . $vName;
                print_r($var);
            } else if (is_string($var)) {
                echo $vType . $vName . '"' . $var . '"';
            } else {
                echo $vType . $vName . $var;
            }
            echo '</div>';
        }
    }
    return false;
}

function myPrintR($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

function getemail($email)
{
    global $db;
    $sql = "SELECT * FROM $db->userTable where  email like '$email'";
    $statement = $db->query($sql);
    $customers = $statement->fetch_all(1);

    if (count($customers) > 0) {
        return $customers[0];
    }
    return false;
}

function allowLoginUser($allow_login_user){
    global $db;
    $sql = "SELECT * FROM $db->userTable where  allow_login_user like '$allow_login_user'";
    $statement = $db->query($sql);
    return $customers = $statement->fetch_all(1);
}
function getuserName($username1)
{
    global $db;
    $sql = "SELECT * FROM $db->userTable where  username like '$username1'";
    $statement = $db->query($sql);
    $customers = $statement->fetch_all(1);


    if (count($customers) > 0) {
        return $customers[0];
    }
    return false;
}


function isValidUsers($username1, $password1, $email, &$errMsg1 = null)
{
    $errMsg1 = '';
    $hasError = false;
    $userEmail = getemail($email);
    if ($userEmail['email'] == $email) {
        $errMsg1 .= "ایمیل وارد شده تکراری است.\n";
        $hasError = true;
    }

    $userName = getuserName($username1);
    if ($userName['username'] == $username1) {
        $errMsg1 .= "نام کاربری وارد شده تکراری است.\n";
        $hasError = true;
    }

    if (strlen($username1) < QA_UNAME_MIN_LENGTH) {
        $errMsg1 .= "نام کاربری وارد شده کوتاه است.\n";
        $hasError = true;
    }

    if (strlen($password1) < U_UPASSWORD_MIN_LENGTH) {
        $errMsg1 .= "تعداد کاراکتر های کلمه عبور کمتر از حد مجاز است.\n";
        $hasError = true;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errMsg1 .= "ایمیل وارد شده نامعتبر است.\n";
        $hasError = true;
    }


    if ($hasError) {
        return false;
    }
    return true;
}


function getUser($username2)
{
    global $db;
    $sql = "SELECT * FROM $db->userTable where  username ='$username2'";
    $statement = $db->query($sql);
    $statements = $statement->fetch_all(1);
    return $statements;
}

function getHash($password1)
{
    $saltStr = 'mohammadDadkhah';
    $hash = sha1($saltStr . md5($password1 . $saltStr));
    return $hash;
}

function isValidUserandAdmin($username2, $password2, &$errMsg = null)
{

    $errMsg = '';
    $hasError = false;
    $user = getUser($username2);
    $passwordHash = getHash($password2);
    if (!($username2 == $user['0']['username'] and $passwordHash == $user['0']['password'])) {
        $errMsg .= "نام کاربری و کلمه عبور اشتباه وارد شده است.\n";
        $hasError = true;
    }
    if ($user['0']['allow_login_user'] == false){
        $errMsg .= "درخواست شما برای وارد شدن به سامانه تایید نشده است.\n";
        $hasError = true;
    }

    if ($hasError) {
        return false;
    }
    return true;
}

function checkUserorAdmin($username2, &$successMsg = null)
{
    if ($username2 == QA_ADMIN_USERNAME) {
        $user = getUser(QA_ADMIN_USERNAME);
        $_SESSION['loginAdmin'] = true;
        $_SESSION['id'] = $user['0']['id'];
        $_SESSION['userIP'] = $_SERVER['REMOTE_ADDR'];
        if (1) {
            return $successMsg .= " ورود مدیر با موفقیت انجام شد.\n";
        }
        return true;
    }


    if ($username2 !== QA_ADMIN_USERNAME) {
        $user = getUser($username2);
        $_SESSION['loginUser'] = true;
        $_SESSION['id'] = $user['0']['id'];
        $_SESSION['username'] = $username2;
        if (1) {
            return $successMsg .= " ورود ارزیاب با موفقیت انجام شد.\n";
        }
        return true;
    }
    return false;
}

/***** Database Functions *****/
function addQuestion($qText, $rid1, $qid, $uid, &$errorMsg3 = '')
{

    global $db;
    // sanitize all inputs in one line !!!
    // list($uName, $uMail, $uMobile, $qText, $rid1, $qid) = array(sanitize($uName), sanitize($uMail), sanitize($uMobile), sanitize($qText), sanitize($rid1), sanitize($qid));
    $sql = "INSERT INTO $db->answerTable ( rid1,text1, qid, uid) VALUES( '$rid1' , '$qText', '$qid', '$uid');";
    $result = $db->query($sql);
    if ($result) {

        $status = 'answered';
        changeQuestionStatus1($qid, $status);
        return true;
    }
    $errorMsg3 = 'خطایی در هنگام ثبت پاسخ شما رخ داده است .';
    return false;
}

function addQuestionAdmin($AdminQuestion, $rid3, $view, &$errorMsg3 = '')
{

    global $db;
    // sanitize all inputs in one line !!!
    // list($uName, $uMail, $uMobile, $qText, $rid1, $qid) = array(sanitize($uName), sanitize($uMail), sanitize($uMobile), sanitize($qText), sanitize($rid1), sanitize($qid));
    $sql = "INSERT INTO $db->answerTable ( text, view) VALUES( '$rid3'  '$AdminQuestion', '$view');";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    $errorMsg3 = 'خطایی در هنگام ثبت پاسخ شما رخ داده است .';
    return false;
}

function addUsers($username1, $password1, $email, &$errorMsg1 = '')
{
    $hashPassword = getHash($password1);
    global $db;
    // sanitize all inputs in one line !!!
    // list($uName, $uMail, $uMobile, $qText, $rid1, $qid) = array(sanitize($uName), sanitize($uMail), sanitize($uMobile), sanitize($qText), sanitize($rid1), sanitize($qid));
    $sql = "INSERT INTO $db->userTable (username, password, email) VALUES('$username1', '$hashPassword', '$email')";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    $errorMsg1 = 'خطایی در هنگام ثبت نام رخ داده است .';
    return false;
}

function addAnswer($qid, $rid1, $aText, &$errorMsg = '')
{
    global $db;

    // sanitize all inputs in one line !!!
    // list($qid, $aText) = array(sanitize($qid), sanitize($aText));
    $sql = "INSERT INTO $db->answerTable ( qid,rid1, text1) VALUES ('$qid','$rid1','$aText');";
    $result = $db->query($sql);
    if ($result) {
        changeQuestionStatus($qid, 'answered');
        // get email of question and send a noification Email ! [using php mail() function]
        // get mobile number of question and send a noification SMS Here ! [using webservice]
        return true;
    }
    $errorMsg = 'خطایی در هنگام ثبت پاسخ شما رخ داده است .';
    return false;
}
function confirmationUser($userLoginAllow, &$errorMsg1 = '')
{
    $allow_login_user=1;
    echo $userLoginAllow;
        global $db;
        $sql = "UPDATE $db->userTable SET allow_login_user='$allow_login_user' WHERE id=$userLoginAllow";
        $result = $db->query($sql);
        if ($result) {
            return true;
        }
    $errorMsg1 = 'خطایی در هنگام تایید ارزیاب رخ داده است .';
    return false;
}

function removeQuestion($qid)
{
    global $db;
    $sql = "Delete from $db->questionTable WHERE id='$qid'";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    return false;
}

function removeAnswer($aid)
{
    global $db;
    $sql = "Delete from $db->answerTable WHERE id='$aid'";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    return false;
}

function removeOverlap($id)
{
    global $db;
    $sql = "Delete from $db->categoriesTable WHERE id='$id' ";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    return false;
}

function getNumQuestions($status = 'all')
{
    global $db;
    if (getValidStatus($status) != 'all') {
        $sql = "SELECT count(*) as c FROM $db->questionTable where status='$status'";
    } else {
        $sql = "SELECT count(*) as c FROM $db->questionTable";
    }
    $result = $db->query($sql);
    if ($result) {
        $record = $result->fetch_object();
        return $record->c;
    }
    return false;
}

function getQuestions($status = 'all', $search = null, $page = 1, &$numQuestions = 0)
{
    global $db;
    // page calculation
    $start = ($page - 1) * QA_QUSETION_PER_PAGE;
    $questionPerPage = QA_QUSETION_PER_PAGE;

    if ($status == 'all') {
        if (!isAdmin()) {
            $whereStr = "status!='pending'";
        } else {
            $whereStr = "1";
        }
        if ($search != null) {
            $sql = "SELECT * FROM $db->questionTable where $whereStr and text like '%$search%' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questionTable where $whereStr and text like '%$search%'";
        } /*else {
            $sql = "SELECT * FROM $db->questionTable where $whereStr order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questionTable where $whereStr";
        }*/
    } elseif (isAdmin() or (in_array(getValidStatus($status), array('publish', 'answered')))) {
        if ($search != null) {
            $sql = "SELECT * FROM $db->questionTable where status='$status' and text like '%$search%' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questionTable where status='$status' and text like '%$search%'";
        } else {
            $sql = "SELECT * FROM $db->questionTable where status='$status' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questionTable where status='$status'";
        }
    } else {
        $sql = "SELECT * FROM $db->questionTable where status!='pending' order by create_date desc limit $start,$questionPerPage";
        $countSql = "SELECT count(*) as c FROM $db->questionTable where status!='pending'";
    }
    $result = $db->query($sql);
    if ($result) {
        $questions = $result->fetch_all(1);
        $numQuestions = $db->query($countSql)->fetch_object()->c;
        return $questions;
    }
    return null;
}

function getQuestionAnsweredVulnerable($view, $rid1, $uid)
{
    global $db;
    $sql1 = "SELECT * FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.rid1='$rid1' and q.view='$view'";

    $result = $db->query($sql1);
    if ($result) {
        $questionsVulnerable = $result->fetch_all(1);
        return $questionsVulnerable;
    }
    return null;

}

function getQuestionPublishVulnerable($status = 'null')
{
    global $db;
    // page calculation
    $sql2 = "SELECT * FROM $db->questionTable where status='$status' ";
    $result = $db->query($sql2);

    if ($result) {
        $questionsVulnerable = $result->fetch_all(1);

        return $questionsVulnerable;
    }
    return null;

}


/*   =======Counts to status Section ============     */
function getcountStatusQuestion($search, $status)
{
    global $db;
    if ($status == 'publish') {
        $countSql = "SELECT count(*) as c FROM $db->questionTable as q where q.status='$status' and q.text  like '%$search%'";
        $numQuestions = $db->query($countSql)->fetch_object()->c;
        return $numQuestions;
    } elseif ($status == 'answered') {
        $countSql = "SELECT count(*) as c FROM $db->questionTable as q where q.status='$status' and q.text  like '%$search%'";
        $numQuestions = $db->query($countSql)->fetch_object()->c;
        return $numQuestions;
    }  elseif ($status == 'pending') {
        $countSql = "SELECT count(*) as c FROM $db->questionTable as q where q.status='$status' and q.text  like '%$search%'";
        $numQuestions = $db->query($countSql)->fetch_object()->c;
        return $numQuestions;
    }


}

function getcountAnswersYesView1Question($search, $uid)
{
    global $db;
    $rid1 = 'بله';
    $view = '1';
    $countSql = "SELECT count(*) as c FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.rid1='$rid1' and q.view='$view' and q.text  like '%$search%'";

    $numQuestions = $db->query($countSql)->fetch_object()->c;
    return $numQuestions;
}

function getcountAnswersNoView1Question($search, $uid)
{
    global $db;
    $rid1 = 'خیر';
    $view = '1';
    $countSql = "SELECT count(*) as c FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.rid1='$rid1' and q.view='$view'  and q.text  like '%$search%'";

    $numQuestions = $db->query($countSql)->fetch_object()->c;
    return $numQuestions;
}

function getcountAnswersYesView0Question($search, $uid)
{
    global $db;
    $rid1 = 'بله';
    $view = '0';
    $countSql = "SELECT count(*) as c FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.rid1='$rid1' and q.view='$view' and q.text  like '%$search%'";

    $numQuestions = $db->query($countSql)->fetch_object()->c;
    return $numQuestions;
}

function getcountAnswersNoView0Question($search, $uid)
{
    global $db;
    $rid1 = 'خیر';
    $view = '0';
    $countSql = "SELECT count(*) as c FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.rid1='$rid1' and q.view='$view' and q.text  like '%$search%'";

    $numQuestions = $db->query($countSql)->fetch_object()->c;
    return $numQuestions;
}

function get_percentage($total, $number)
{
    if ($total > 0) {
        return round($number * ($total / 100), 2);
    } else {
        return 0;
    }
}

/*   =======Counts Section ============     */

function getNumberAllNetwork($search)
{
    global $db;
    $whereStr = "1";
    if ($search != null) {
        $countSql_network = "SELECT count(*) as c FROM $db->questionTable where $whereStr and text like '%$search%'";
        $numQuestions_network = $db->query($countSql_network)->fetch_object()->c;
        return $numQuestions_network;
    } else {
        $countSql_all = "SELECT count(*) as c FROM $db->questionTable where $whereStr ";
        $numQuestions_all = $db->query($countSql_all)->fetch_object()->c;
        return $numQuestions_all;
    }

}

/* تعداد کل چک لیست ها */
$numQuestions_all = getNumberAllNetwork(null);
/* تعداد کل چک لیست های بخش شبکه */
$numQuestions_network = getNumberAllNetwork('(امضاهای آسیب پذیر)') + getNumberAllNetwork("(ایجاد کلید ناقص)")
    + getNumberAllNetwork("(حمله منع سرویس توزیع شده)") + getNumberAllNetwork("(حمله زمان ربایی)")
    + getNumberAllNetwork("(حمله مسیریابی)") + getNumberAllNetwork("(حمله سیبیل)")
    + getNumberAllNetwork("(حمله فیشینگ)") + getNumberAllNetwork("(حمله لغت نامه)")
    + getNumberAllNetwork("(حمله باز پخش)");

/* تعداد کل چک لیست های بخش نرم افزار */
$numQuestions_software = getNumberAllNetwork('(حمله فینی)') + getNumberAllNetwork('(حمله رقابتی)')
    + getNumberAllNetwork('(حمله تاریخ جایگزین)') + getNumberAllNetwork('(حمله 51 درصد)')
    + getNumberAllNetwork('(حمله شکل پذیری تراکنش)') + getNumberAllNetwork('(حمله خسوف)')
    + getNumberAllNetwork('(آسیب پذیری در کد منبع قرارداد)') + getNumberAllNetwork('(آسیب پذیری در ماشین مجازی)')
    + getNumberAllNetwork('(استخراج خودخواهانه)') + getNumberAllNetwork('(فورک پس از مضایقه)');

/* تعداد کل چک لیست های بخش سخت افزار */
$numQuestions_hardware = getNumberAllNetwork('(لایه های شبکه)') + getNumberAllNetwork('(کیف پول سرد)')
    + getNumberAllNetwork('(کیف پول گرم)') + getNumberAllNetwork('(الگوریتم های اجماع)');

/*   =======Counts Section ============     */
function getAnswers($qid, $uid)
{

    global $db;
    $sql = "SELECT * FROM $db->answerTable where qid='$qid' and uid='$uid'";
    $result = $db->query($sql);
    if ($result) {
        $answers = $result->fetch_all(1);
        //Print_r( $answers);
        $str = '';
        //
        foreach ($answers as $a) {
            $str .= '<div style="text-align=right" class="a" id="a-' . $a['id1'] . '">' . "(" . "پاسخ شماره" . nl2br($a['id1']) . ")" . nl2br($a['rid1']) . " " . nl2br($a['text1']) . '<span class="date">(' . " " . " " . jdate(QA_DATE_FORMAT, strtotime($a['create_date'])) . ')</span></div>' . PHP_EOL;
        }
        return $str;
    }
    return '';
}

function getAnswersVulnerable($qid, $uid)
{

    global $db;
    $sql = "SELECT * FROM $db->questionTable as q, $db->answerTable as a  where a.qid=q.id and a.uid='$uid' and  a.qid='$qid'";
    $result = $db->query($sql);
    if ($result) {
        $answers = $result->fetch_all(1);
        //Print_r( $answers);
        $str = '';
        //
        foreach ($answers as $a) {

            $str .= '<div style="text-align=right" class="a" id="a-' . $a['id'] . '">' . "(" . "پاسخ شماره" . nl2br($a['id']) . ")" . nl2br($a['rid1']) . " " . nl2br($a['text1']) . '<span class="date">(' . " " . " " . jdate(QA_DATE_FORMAT, strtotime($a['create_date'])) . ')</span></div>' . PHP_EOL;

        }
        return $str;
    }
    return '';
}

function getNumberQuestions($numberQuestion)
{
    global $db;
    if ($numberQuestion != null) {
        $sql = "SELECT * FROM $db->questionTable where id='$numberQuestion'";
        $result = $db->query($sql);

        if ($result) {
            $questions = $result->fetch_all(1);
            return $questions;
        }
    }
    return null;
}


function addCategories($qid, $rid2, $Overlap_id, &$errorMsg3 = '')
{

    global $db;
    // sanitize all inputs in one line !!!
    // list($uName, $uMail, $uMobile, $qText, $rid1, $qid) = array(sanitize($uName), sanitize($uMail), sanitize($uMobile), sanitize($qText), sanitize($rid1), sanitize($qid));
    $sql = "INSERT INTO $db->categoriesTable ( cat_id,name_attack,Overlap_id)  VALUES(  '$qid','$rid2','$Overlap_id')";
    $result = $db->query($sql);
    if ($result) {
        return true;

    }
    $errorMsg3 = 'خطایی در هنگام ثبت سوال های هم پوشانی رخ داده است .';
    return false;
}

$categories = null;
function getCategories($search1)
{

    global $db;
    if ($search1 !== null) {
        $sql = "SELECT * FROM $db->categoriesTable where cat_id='$search1'";
        $result = $db->query($sql);
        if ($result) {
            $categories = $result->fetch_all(1);

            $str = '';
            //
            foreach ($categories as $c) {

                $str .=

                    '<tbody>' .
                    '<tr>'
                    . '<td >' . nl2br($c['id']) . '</td>'
                    . '<td >' . nl2br($c['cat_id']) . '</td>'
                    . '<td colspan="70" style="width: 50%;text-align: center">' . nl2br($c['name_attack']) . '</td>'
                    . '<td style="text-align: center">' . nl2br($c['Overlap_id']) . '</td>'
                    . '</tr>'
                    . '</tbody>' . PHP_EOL;

            }
            return $str;
        }

    }
}


// myPrint( $answers);
//  $str = '';

/* foreach ($categories as $c) {

     $str .=
         '<tbody>' .
         '<tr>'
         .'<td >' . nl2br($c['id']) . '</td>' .

          '<td >' . nl2br($c['cat_id']) . '</td>'

         . '<td colspan="70" style="width: 50%;text-align: center">' . nl2br($c['name_attack']) . '</td>'
         . '<td style="text-align: center">' . nl2br($c['Overlap_id']) . '</td>'

         . '</tr>'
         . '</tbody>' . PHP_EOL;
 }*/


/*    $sql2 = "SELECT * FROM $db->answerTable where qid='$c['Overlap_id']' and uid='$uid'";
    $result2 = $db->query($sql2);
    if ($result2)
 $answers = $result2->fetch_all(1);
    foreach ($answers as $a) {
        $str .=
            '<td>' . nl2br($a['rid1']) . '</td>'
            . PHP_EOL;
    }*/

/*$answ='';
$answe=getCategories($qid,$uid);
function getCategoriesanswers($answe){

}*/
function strReplace($search)
{
    return str_ireplace('%', ' ', $search);
}

function changeQuestionStatus($qid, $status)
{
    if (isValidStatus($status)) {
        global $db;
        $sql = "UPDATE $db->questionTable SET status='$status' WHERE id=$qid;";
        $result = $db->query($sql);
        if ($result) {
            return true;
        }
    }
    return false;
}

function changeQuestionStatus1($qid, $status)
{
    global $db;
    $sql = "UPDATE $db->questionTable SET status='$status' WHERE id=$qid;";
    $result = $db->query($sql);
    if ($result) {
        return true;
    }
    return false;
}


/***** Authentication(login/logout/check) Functions *****/


function doLogout()
{
    unset($_SESSION['loginAdmin'], $_SESSION['loginUser'], $_SESSION['user'], $_SESSION['id'], $_SERVER['REMOTE_ADDR']);
    return true;
}

function isAdmin()
{
    return (isset($_SESSION['loginAdmin'])) ? true : false;
}

function isUser()
{
    return (isset($_SESSION['loginUser'])) ? true : false;
}

/***** Data Cleaning and Sanitizing Functions *****/
/*function cleanInput(&$input)
{

    $search = array(
        '@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@' // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    $input = $output;
    return $output;
}*/

/*function sanitize(&$input)
{
    if (is_array($input)) {
        foreach ($input as $var => $val) {
            $output[$var] = sanitize($val);
        }
    } else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input = cleanInput($input);
        $output = mysql_real_escape_string($input);
    }
    $input = $output;
    return $output;
}*/

/***** Pagination Functions *****/
function getNumPages($numQuestions)
{
    $numPages = ceil($numQuestions / QA_QUSETION_PER_PAGE);
    return $numPages;
}

function getPageUrl($pageNumber)
{
    $getParameters = array();
    if (isset($_GET['status']))
        $getParameters['status'] = $_GET['status'];
    if (isset($_GET['search']))
        $getParameters['search'] = $_GET['search'];
    $getParameters['page'] = $pageNumber;
    $str = '?';
    foreach ($getParameters as $key => $value) {
        $str .= "$key=$value&";
    }
    if (isset($_GET['search'])) {
        if ($_GET['search'] == '(کیف پول سرد)' or $_GET['search'] == '(کیف پول گرم)' or $_GET['search'] == '(الگوریتم های اجماع)' or $_GET['search'] == '(لایه های شبکه)') {
            return QA_HARDWAREPAGE_URL . trim($str, '&');
        } elseif ($_GET['search'] == '(حمله فینی)' or $_GET['search'] == '(حمله رقابتی)' or $_GET['search'] == '(حمله تاریخ جایگزین)' or $_GET['search'] == '(حمله 51 درصد)' or $_GET['search'] == '(حمله شکل پذیری تراکنش)' or $_GET['search'] == '(حمله خسوف)' or $_GET['search'] == '(آسیب پذیری در کدمنبع قرارداد)' or $_GET['search'] == '(آسیب پذیری در ماشین مجازی)' or $_GET['search'] == '(استخراج خودخواهانه)' or $_GET['search'] == '(فورک پس از مضایقه)') {
            return QA_NETWORKPAGE_URL . trim($str, '&');
        } elseif ($_GET['search'] == '(امضاهای آسیب پذیر)' or $_GET['search'] == '(ایجاد کلید ناقص)' or $_GET['search'] == '(حمله منع سرویس توزیع شده)' or $_GET['search'] == '(حمله زمان ربایی)' or $_GET['search'] == '(حمله مسیر یابی)' or $_GET['search'] == '(حمله سیبیل)' or $_GET['search'] == '(حمله فیشینگ)' or $_GET['search'] == '(حمله لغت نامه)' or $_GET['search'] == '(حمله باز پخش)') {
            return QA_SOFTWAREPAGE_URL . trim($str, '&');
        }
    }
}


