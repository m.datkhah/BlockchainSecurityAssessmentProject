<?php include_once 'assets/inc/actions.php'; ?>
<?php include_once 'assets/inc/config.php'; ?>
<!DOCTYPE html>
<html lang="fn">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!--  codes project base-->
    <link rel="stylesheet" href="assets/css/pure.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
    <!-- end codes project base-->
  <!--    از فایل login.php-->
  <link rel="stylesheet" href="assets/css/pure.css" type="text/css"/>
  <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>

  <title>Inner Page - eNno Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
 <!-- <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">-->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: eNno - v4.0.1
  * Template URL: https://bootstrapmade.com/enno-free-simple-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
      <div class="container d-flex align-items-center justify-content-between">


          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

          <nav id="navbar" class="navbar font-type">
              <ul>
                  <li><a class="nav-link scrollto active" href="<?php echo QA_HOME_URL . 'software-page.php.php#hero'; ?>">خانه</a></li>
                  <li class="dropdown"><a href="<?php echo QA_HOME_URL.'software-page.php' . '#breadcrumbs'; ?>"><span>چک لیست ها</span> <i class="bi bi-chevron-down"></i></a>
                  </li>
                  <li><a class="nav-link scrollto " href="<?php echo QA_HOME_URL . 'software-page.php#featured-services1'; ?>">ارزیابی فنی چک لیست ها</a></li>
                  <li><a class="nav-link scrollto" href="<?php echo QA_HOME_URL . 'index.php#team'; ?>">درباره ما</a></li>
                  <?php if (isUser()): ?>
                      <li> <a class="logout" href="<?php echo QA_HOME_URL . '?logout=1'; ?>">خروج</a></li>
                      <div class="info">
                          <span class="adminName">خوش آمدی <?php echo $_SESSION['username']; ?> عزیز</span>
                      </div>
                  <?php endif; ?>
                  <?php if (isAdmin()): ?>
                      <li> <a class="logout" href="<?php echo QA_HOME_URL . '?logout=1'; ?>">خروج</a></li>
                      <div class="info">
                          <span class="adminName">خوش آمدی <?php echo QA_ADMIN_DISPLAYNAME; ?> عزیز</span>
                      </div>
                  <?php endif; ?>

                  <!--          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>-->
                  <!--          <li><a class="getstarted scrollto" href="#about">Get Started</a></li>-->
              </ul>
              <i class="bi bi-list mobile-nav-toggle"></i>
          </nav><!-- .navbar -->
          <h1 class="logo"><a href="index.html"><img src="assets/img/download.png"></a></h1>
      </div>
  </header><!-- End Header -->
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

      <div class="container">
          <div class="row ">
              <div class=" col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center ">
                  <h1 class="font-type" ><?php echo QA_TITLE; ?></h1>
                  <h2 style="text-align: right;" class="font-type"><?php echo QA_TITLE_DESCRIPTION; ?></h2>
                  <div class="d-flex font-type">
                      <a href="#about" class="btn-get-started scrollto ">شروع به کار</a>
                  </div>
              </div>
              <div class="col-lg-6 order-1 order-lg-2 hero-img">
                  <img src="assets/img/blockchain-technology.jpg" class="img-fluid animated" alt="">
              </div>
          </div>
      </div>

  </section><!-- End Hero -->
  <!-- ======= Counts Section ======= -->
  <section id="counts" class="counts">
      <div class="container">

          <div class="row counters">
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php
                          $numQuestions_software=getNumberAllNetwork('(حمله فینی)') + getNumberAllNetwork('(حمله رقابتی)')
                              + getNumberAllNetwork('(حمله تاریخ جایگزین)') + getNumberAllNetwork('(حمله 51 درصد)')
                              + getNumberAllNetwork('(حمله شکل پذیری تراکنش)') + getNumberAllNetwork('(حمله خسوف)')
                              + getNumberAllNetwork('(آسیب پذیری در کد منبع قرارداد)') + getNumberAllNetwork('(آسیب پذیری در ماشین مجازی)')
                              + getNumberAllNetwork('(استخراج خودخواهانه)') + getNumberAllNetwork('(فورک پس از مضایقه)');

                          echo $numQuestions_software; ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد کل چک لیست های بخش نرم افزار</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php
                          $countPendingQuestion= getcountStatusQuestion('(حمله فینی)','pending') + getcountStatusQuestion('(حمله رقابتی)','pending')
                                               + getcountStatusQuestion('(حمله تاریخ جایگزین)','pending') + getcountStatusQuestion('(حمله 51 درصد)','pending')
                                               + getcountStatusQuestion('(حمله شکل پذیری تراکنش)','pending') + getcountStatusQuestion('(حمله خسوف)','pending')
                                               + getcountStatusQuestion('(آسیب پذیری در کد منبع قرارداد)','pending') + getcountStatusQuestion('(آسیب پذیری در ماشین مجازی)','pending')
                                               + getcountStatusQuestion('(استخراج خودخواهانه)','pending') + getcountStatusQuestion('(فورک پس از مضایقه)','pending');

                          echo $countPendingQuestion; ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد کل چک لیست های منتظر تایید مدیر بخش نرم افزار</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php
                          $countPublishQuestionSoftware = getcountStatusQuestion('(حمله فینی)', 'publish') + getcountStatusQuestion('(حمله رقابتی)', 'publish')
                              + getcountStatusQuestion('(حمله تاریخ جایگزین)', 'publish') + getcountStatusQuestion('(حمله 51 درصد)', 'publish')
                              + getcountStatusQuestion('(حمله شکل پذیری تراکنش)', 'publish') + getcountStatusQuestion('(حمله خسوف)', 'publish')
                              + getcountStatusQuestion('(آسیب پذیری در کد منبع قرارداد)', 'publish') + getcountStatusQuestion('(آسیب پذیری در ماشین مجازی)', 'publish')
                              + getcountStatusQuestion('(استخراج خودخواهانه)', 'publish') + getcountStatusQuestion('(فورک پس از مضایقه)', 'publish');
                          $countansweredQuestionSoftware = getcountStatusQuestion('(حمله فینی)', 'answered') + getcountStatusQuestion('(حمله رقابتی)', 'answered')
                              + getcountStatusQuestion('(حمله تاریخ جایگزین)', 'answered') + getcountStatusQuestion('(حمله 51 درصد)', 'answered')
                              + getcountStatusQuestion('(حمله شکل پذیری تراکنش)', 'answered') + getcountStatusQuestion('(حمله خسوف)', 'answered')
                              + getcountStatusQuestion('(آسیب پذیری در کد منبع قرارداد)', 'answered') + getcountStatusQuestion('(آسیب پذیری در ماشین مجازی)', 'answered')
                              + getcountStatusQuestion('(استخراج خودخواهانه)', 'answered') + getcountStatusQuestion('(فورک پس از مضایقه)', 'answered');
                          $countPublishQuestionSoftware=$countPublishQuestionSoftware+$countansweredQuestionSoftware;
                          echo $countPublishQuestionSoftware; ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد کل چک لیست های تایید شده در بخش نرم افزار</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله فینی)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله فینی</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <link rel="stylesheet" href="assets/css/w3.css">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله رقابتی)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله رقابتی</p>
              </div>

              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله تاریخ جایگزین)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله تاریخ جایگزین</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله 51 درصد)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله 51 درصد</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله شکل پذیری تراکنش)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله شکل پذیری تراکنش</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(حمله خسوف)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های حمله خسوف</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(آسیب پذیری در کد منبع قرارداد)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های آسیب پذیری در کد منبع قرارداد</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(آسیب پذیری در ماشین مجازی)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های آسیب پذیری در ماشین مجازی</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(استخراج خودخواهانه)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های استخراج خودخواهانه</p>
              </div>
              <div class="col-lg-3 col-6 text-center">
                  <div class="w3-container w3-center w3-animate-zoom">
                      <h1 style="color: #1c7430"><?php echo getNumberAllNetwork('(فورک پس از مضایقه)'); ?></h1>
                  </div>
                  <p style="text-align: center" class="font-type">تعداد چک لیست های فورک پس از مضایقه</p>
              </div>

          </div>

      </div>
  </section><!-- End Counts Section -->
      <!-- ======= about ======= -->
      <div id="about" class="about container" style="direction: rtl;">
          <div class="container" style="direction: rtl;">
          <!-- ======= Breadcrumbs ======= -->
              <section id="breadcrumbs" class="breadcrumbs">
                  <div class="container">
                      <div class="container">
                          <div class="content icon-box1 font-type ">
                              <form id="form" action=" " method="get" class="pure-form searchform font-type">
                                  <select name="status" id="cars">
                                      <option label="وضعیت سوال مورد نظر را انتخاب کنید:"></option>
                                      <option value="all">همه ی سوالات</option>
                                      <?php if (isAdmin()): ?>
                                          <option value="pending">منتظر تائید</option>
                                      <?php endif; ?>
                                      <option value="publish">بدون پاسخ</option>
                                      <option value="answered">پاسخ داده شده</option>
                                  </select>
                                  <select name="search" id="cars">
                                      <option label="حمله مورد نظر را انتخاب کنید..." ></option>
                                      <optgroup label="امنیت نرم افزار">
                                          <optgroup label="امنیت تراکنش ها">
                                              <option value="(حمله فینی)">حمله فینی</option>
                                              <option value="(حمله رقابتی)">حمله رقابتی</option>
                                              <option value="(حمله تاریخ جایگزین)">حمله تاریخ جایگزین</option>
                                              <option value="(حمله 51 درصد)">حمله 51 درصد</option>
                                              <option value="(حمله شکل پذیری تراکنش)">حمله شکل پذیری تراکنش</option>
                                              <option value="(حمله خسوف)">حمله خسوف</option>
                                          </optgroup>
                                          <optgroup label="حمله های قرارداد هوشمند">
                                              <option value="(آسیب پذیری در کد منبع قرارداد)">آسیب پذیری در کد منبع
                                                  قرارداد
                                              </option>
                                              <option value="(آسیب پذیری در ماشین مجازی)">آسیب پذیری در ماشین مجازی
                                              </option>
                                          </optgroup>
                                          <optgroup label="حمله های استخرهای استخراج">
                                              <option value="(استخراج خودخواهانه)">استخراج خودخواهانه</option>
                                              <option value="(فورک پس از مضایقه)">فورک پس از مضایقه</option>
                                          </optgroup>
                                      </optgroup>
                                  </select>

                                   <input class="pure-button button-green font-type" type="submit" value="جستجو">
                              </form><br>
                              <form id="form" action=" " method="get" class="pure-form searchform font-type">
                                  <input style="margin-right: -218px;width: 20%" type="number" id="quantity" name="numberQuestion" min="1" max="240" placeholder="شماره سوال مورد نظر را وارد کنید:">
                                  <input style="margin-left: 12px" class="pure-button button-green font-type" type="submit" value="جستجو">
                              </form>
                          </div>
                      </div>

                  </div>


                      <!--  modal box- submit response-->
                      <style>
                          ul.breadcrumb {
                              padding: 10px 16px;
                              list-style: none;
                              background-color: #eee;
                              direction: rtl;
                          }
                          ul.breadcrumb li {
                              display: inline;
                              font-size: 18px;
                          }
                          ul.breadcrumb li+li:before {
                              padding: 8px;
                              color: black;
                              content: "/\00a0";
                          }
                          ul.breadcrumb li a {
                              color: #0275d8;
                              text-decoration: none;
                          }
                          ul.breadcrumb li a:hover {
                              color: #01447e;
                              text-decoration: underline;
                          }
                      </style>

                      <ul class="breadcrumb">
                          <li><a href="#">خانه</a></li>
                          <li><a href="#">چک لیست ها</a></li>
                          <li><a href="<?php echo QA_HOME_URL . 'index.php#about1'; ?>">چک لیست های بخش نرم افزار</a></li>
                          <li>
                             <?php
                        echo strReplace($search) ; ?></li>
                      </ul>
                  <div style="direction: ltr">

                      <?php if (isAdmin()): ?>
                          <button type="button"  class="btn btn-light border rounded-pill shadow-sm mb-1" data-toggle="modal" data-target="#right_modal1"><i  class="">  </i>ثبت سوال های هم پوشانی   </button>
                          <button type="button" class="btn btn-light border rounded-pill shadow-sm mb-1"
                                  data-toggle="modal" data-target="#right_modal2"><i class=""> </i>حذف سوال های هم پوشانی
                          </button>
                          <button type="button" class="btn btn-light border rounded-pill shadow-sm mb-1" data-toggle="modal" data-target="#regular_modal">ثبت سوال <i class="fa fa-window-maximize pl-2"></i></button>
                      <?php endif; ?>
                  </div>

                  <?php
                  if ($errorMsg3) {
                      echo "<div class='error'>" . nl2br($errorMsg3) . "</div>";
                  } elseif ($successMsg3) {
                      echo "<div class='success'>" . nl2br($successMsg3) . "</div>";
                  }
                  ?>

                  <div style="text-align: right" class="qTitle font-type"> لیست سوالات مطرح شده
                      :(<?php echo $numQuestions; ?> مورد)
                  </div>
                  <?php
                  if ($questions == null) {
                      echo '
                        <div class="notfound">موردی یافت نشد
                       
                        </div>';
                  } else {
                  ?>
          </section><!-- End Breadcrumbs -->
          <div class="container" id="checklist">


              <!--  ===========================================================                 -->

              <div class="modal modal-right fade" id="right_modal2" tabindex="-1" role="dialog"
                   aria-labelledby="right_modal">
                  <div class="modal-dialog" role="document" style="height:69%; position: initial; margin-top: 20px">
                      <div class="modal-content">
                          <div class="modal-header">
                              <h5 class="modal-title">فرم حذف سوال های هم پوشانی</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>

                          </div>
                          <div class="modal-body">
                              <!-- <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                               <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                               <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>-->
                              <!--      فرم ثبت پاسخ              -->


                              <div>
                                  <div class="sidebar">
                                      <div class="inner">
                                          <div class="menu">
                                              <div class="menu-title">حذف سوال های هم پوشانی :</div>
                                              <div class="menu-content">
                                                  <form action="" method="post" class="pure-form searchform">
                                                      <input type="number" id="quantity" name="id" min="1" max="240"
                                                             placeholder="شماره سطر سوال را وارد کنید">
                                                      <input type="submit" name="removeCategories"
                                                             value="حذف سوال های هم پوشانی"
                                                             class="pure-button ">
                                                  </form>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <!--     فرم ثبت پاسخ پایان               -->
                          </div>
                          <!-- <div class="modal-footer modal-footer-fixed">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary">Save changes</button>
                           </div>-->
                      </div>
                  </div>
              </div>
              <script src="assets/js/jquery.min.js"></script>
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
              <!-- End modal box- submit response-->
              <!--     فرم ثبت سوال های هم پوشانی               -->
              <div class="modal modal-right fade" id="right_modal1" tabindex="-1" role="dialog" aria-labelledby="right_modal">
                  <div class="modal-dialog" role="document" style="height:69%; position: initial; margin-top: 20px" >
                      <div class="modal-content">
                          <div class="modal-header">
                              <h5 class="modal-title">فرم ثبت سوال های هم پوشانی</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>

                          </div>
                          <div class="modal-body">
                              <!-- <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                               <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                               <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>-->
                              <!--      فرم ثبت پاسخ              -->


                              <div >
                                  <div class="sidebar">
                                      <div class="inner">
                                          <div class="menu">
                                              <div class="menu-title">طرح سوال های هم پوشانی :</div>
                                              <div class="menu-content">
                                                  <form action="" method="post" class="pure-form searchform">
                                                      <input type="number" id="quantity" name="qid" min="1" max="240"   placeholder="شماره سوال">
                                                      <!--       section                                     -->
                                                      <div  >
                                                          <select class="custom-select ltr" name="rid2">
                                                              <option selected label="انتخاب کنید..."></option>
                                                              <optgroup label="امنیت سخت افزار">
                                                                  <option value="امنیت سخت افزار / کیف پول سرد">کیف پول
                                                                      سرد
                                                                  </option>
                                                                  <option value="امنیت سخت افزار / کیف پول گرم"> کیف پول
                                                                      گرم
                                                                  </option>
                                                                  <option value="امنیت سخت افزار / الگوریتم های اجماع">
                                                                      الگوریتم های اجماع
                                                                  </option>
                                                                  <option value="امنیت سخت افزار / لایه های شبکه">لایه های
                                                                      شبکه
                                                                  </option>
                                                              </optgroup>
                                                              <optgroup label="امنیت نرم افزار">
                                                                  <optgroup label="امنیت تراکنش ها">
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله فینی">
                                                                          حمله فینی
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله رقابتی">
                                                                          حمله رقابتی
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله تاریخ جایگزین">
                                                                          حمله تاریخ جایگزین
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله 51 درصد">
                                                                          حمله 51 درصد
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله شکل پذیری تراکنش">
                                                                          حمله شکل پذیری تراکنش
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / امنیت تراکنش ها / حمله خسوف">
                                                                          حمله خسوف
                                                                      </option>
                                                                  </optgroup>
                                                                  <optgroup label="حمله های قرارداد هوشمند">
                                                                      <option value="امنیت نرم افزار / حمله های قرارداد هوشمند / آسیب پذیری در کد منبع قرارداد">
                                                                          آسیب پذیری در کد منبع قرارداد
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / حمله های قرارداد هوشمند / آسیب پذیری در ماشین مجازی">
                                                                          آسیب پذیری در ماشین مجازی
                                                                      </option>
                                                                  </optgroup>
                                                                  <optgroup label="حمله های استخرهای استخراج">
                                                                      <option value="امنیت نرم افزار / حمله های استخرهای استخراج /استخراج خودخواهانه">
                                                                          استخراج خودخواهانه
                                                                      </option>
                                                                      <option value="امنیت نرم افزار / حمله های استخرهای استخراج /فورک پس از مضایقه">
                                                                          فورک پس از مضایقه
                                                                      </option>
                                                                  </optgroup>
                                                              </optgroup>
                                                              <optgroup label="امنیت شبکه">
                                                                  <option value="امنیت شبکه / امضاهای آسیب پذیر">امضاهای
                                                                      آسیب پذیر
                                                                  </option>
                                                                  <option value="امنیت شبکه / ایجاد کلید ناقص">ایجاد کلید
                                                                      ناقص
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله منع سرویس توزیع شده">
                                                                      حمله منع سرویس توزیع شده
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله زمان ربایی">حمله زمان
                                                                      ربایی
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله مسیریابی">حمله
                                                                      مسیریابی
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله سیبیل">حمله سیبیل
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله فیشینگ">حمله فیشینگ
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله لغت نامه">حمله لغت
                                                                      نامه
                                                                  </option>
                                                                  <option value="امنیت شبکه / حمله باز پخش">حمله باز پخش
                                                                  </option>
                                                              </optgroup>
                                                          </select>
                                                      </div>
                                                      <input type="number" id="quantity" name="Overlap_id"  min="1" max="240"  placeholder="شماره سوال های هم پوشانی را وارد کنید" >
                                                   <!--   <input type="hidden"  name="uid" value="<?php /*echo $_SESSION['id'];*/?>">-->
                                                      <input type="submit" name="submitCategories"  value="ارسال سوال های هم پوشانی"
                                                             class="pure-button ">
                                                  </form>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <!--     فرم ثبت پاسخ پایان               -->
                          </div>
                          <!-- <div class="modal-footer modal-footer-fixed">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary">Save changes</button>
                           </div>-->
                      </div>
                  </div>
              </div>

              <script src="assets/js/jquery.min.js"></script>
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
              <!-- End modal box- submit response-->
              <!--     پایان فرم ثبت سوال های هم پوشانی               -->
              <!-- End modal box- submit response-->
              <!--modal box- submit manage admin checklist-->
              <div class="modal fade" id="regular_modal" tabindex="-1" role="dialog" aria-labelledby="regular_modal">
                  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                      <div class="modal-content" style="margin: auto;">
                          <div class="modal-header">
                              <h5 class="modal-title">ثبت سوال</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              <form method="get" action="">
                                  <style>

                                      .comment {
                                          float: right;
                                          width: 100%;
                                          height: auto;
                                      }

                                      .commenter {
                                          float: right;
                                      }

                                      .commenter img {
                                          width: 35px;
                                          height: 35px;
                                      }

                                      .comment-text-area {
                                          float: left;
                                          width: 100%;
                                          height: auto;
                                      }

                                      .textinput {
                                          float: left;
                                          width: 100%;
                                          min-height: 75px;
                                          outline: none;
                                          resize: none;
                                          border: 1px solid grey;
                                      }
                                  </style>
                                  <div>
                                      <div class="comment">
                                          <select class="custom-select ltr font-type" name="view">
                                              <option selected label="نگرش طرح سوال را انتخاب کنید..."></option>
                                              <option  value="1">نگرش مثبت</option>
                                              <option  value="0">نگرش منفی</option>
                                          </select>
                                          <select class="custom-select ltr font-type" name="rid3">
                                              <option selected label="بخش مورد نظر را انتخاب کنید..."></option>
                                              <optgroup label="امنیت سخت افزار">
                                                  <option value="(کیف پول سرد)">کیف پول
                                                      سرد
                                                  </option>
                                                  <option value="(کیف پول گرم)"> کیف پول
                                                      گرم
                                                  </option>
                                                  <option value="(الگوریتم های اجماع)">
                                                      الگوریتم های اجماع
                                                  </option>
                                                  <option value="(لایه های شبکه)">لایه های
                                                      شبکه
                                                  </option>
                                              </optgroup>
                                              <optgroup label="امنیت نرم افزار">
                                                  <optgroup label="امنیت تراکنش ها">
                                                      <option value="(حمله فینی)">
                                                          حمله فینی
                                                      </option>
                                                      <option value="(حمله رقابتی)">
                                                          حمله رقابتی
                                                      </option>
                                                      <option value="(حمله تاریخ جایگزین)">
                                                          حمله تاریخ جایگزین
                                                      </option>
                                                      <option value="(حمله 51 درصد)">
                                                          حمله 51 درصد
                                                      </option>
                                                      <option value="(حمله شکل پذیری تراکنش)">
                                                          حمله شکل پذیری تراکنش
                                                      </option>
                                                      <option value="(حمله خسوف)">
                                                          حمله خسوف
                                                      </option>
                                                  </optgroup>
                                                  <optgroup label="حمله های قرارداد هوشمند">
                                                      <option value="(آسیب پذیری در کد منبع قرارداد)">
                                                          آسیب پذیری در کد منبع قرارداد
                                                      </option>
                                                      <option value="(آسیب پذیری در ماشین مجازی)">
                                                          آسیب پذیری در ماشین مجازی
                                                      </option>
                                                  </optgroup>
                                                  <optgroup label="حمله های استخرهای استخراج">
                                                      <option value="(استخراج خودخواهانه)">
                                                          استخراج خودخواهانه
                                                      </option>
                                                      <option value="(فورک پس از مضایقه)">
                                                          فورک پس از مضایقه
                                                      </option>
                                                  </optgroup>
                                              </optgroup>
                                              <optgroup label="امنیت شبکه">
                                                  <option value="(امضاهای آسیب پذیر)">امضاهای
                                                      آسیب پذیر
                                                  </option>
                                                  <option value="(ایجاد کلید ناقص)">ایجاد کلید
                                                      ناقص
                                                  </option>
                                                  <option value="(حمله منع سرویس توزیع شده)">
                                                      حمله منع سرویس توزیع شده
                                                  </option>
                                                  <option value="(حمله زمان ربایی)">حمله زمان
                                                      ربایی
                                                  </option>
                                                  <option value="(حمله مسیریابی)">حمله
                                                      مسیریابی
                                                  </option>
                                                  <option value="(حمله سیبیل)">حمله سیبیل
                                                  </option>
                                                 <option value="(حمله فیشینگ)">حمله فیشینگ
                                                  </option>
                                                  <option value="(حمله لغت نامه)">حمله لغت
                                                      نامه
                                                  </option>
                                                  <option value="(حمله باز پخش)">حمله باز پخش
                                                  </option>
                                              </optgroup>
                                          </select>
                                          <textarea name="AdminQuestion" class="textinput font-type"
                                                    placeholder="سوال را مطابق دسته بندی انجام شده، وارد کنید"></textarea>
                                      </div>
                                  </div>

                                  <!--                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                                  <button name="SubmitAdminQuestion" type="submit" class="btn btn-primary">ارسال سوال</button>
                          </div>
                          </form>
                          <!--                    <a href="#" class="btn btn-primary">1</a> <a href="#" class="btn btn-success">2</a> <a href="#" class="btn btn-danger">3</a> <a href="#" class="btn btn-warning">4</a> <a href="#" class="btn btn-info">5</a> <a href="#" class="btn btn-secondary">6</a> <a href="#" class="btn btn-dark">7</a>-->
                      </div>
                  </div>
              </div>
          </div>
          <!-- End modal box- submit manage admin checklist-->

          <!-- modal box- submit manage admin checklist response-->


              <!-- End modal box- overlap checklist-->
          <div class="inner content">

              <?php foreach ($questions as $q): ?>
                  <div class="question <?php echo $q['status'] ?>" id="q-<?php echo $q['id'] ?>">

                          <div class="qManage br5">
                              <?php if (isAdmin()): ?>
                              <span class="result" style="display: none">...</span>
                              <span class="qm" id="qmd-<?php echo $q['id'] ?>">حذف</span> &nbsp; &nbsp;
                              <?php if ($q['status'] != 'answered') { ?>
                                  <span class="qm" id="qmpu-<?php echo $q['id'] ?>">تائید</span> &nbsp; &nbsp;
                                  <span class="qm" id="qmpe-<?php echo $q['id'] ?>">لغو تائید</span>
                              <?php } ?>
                              <?php endif; ?>
                          </div>

                      <div class="q"><br><br><br><span
                                  class="i">+</span><?php echo "سوال شماره ی" . $q['id'] . " : ", "<br>" . $q['text'] ?>
                          <span class="date">(<?php echo $q['uname'] . '  در ' . jdate(QA_DATE_FORMAT, strtotime($q['create_date'])); ?>)</span>
                      </div>
                      <?php
                      if ($q['status'] == 'answered') {
                          echo getAnswers($q['id'],$_SESSION['id']);
                      } else if ($q['status'] == 'publish') {
                          echo '<div style="text-align: right" class="a">پاسخی برای این سوال داده نشده است !</div>';
                      } else if ($q['status'] == 'pending' and isAdmin()) {
                          echo '<div style="text-align: right" class="a">این سوال هنوز تائید نشده است.</div>';
                      }
                      ?>
                      <div id="overlap-<?php echo $q['id'] ?>" class="overlap" style="margin-top: -81px">
                          <div class="q"><br><br><br><span
                                      class="i">+</span><?php echo "سوال های هم پوشانی با سوال شماره ی" . $q['id'] . " : " ?>
                          </div>
                          <div class="r"></div>
                          <table style="display: none; direction: rtl" class="a">

                              <?php
                              echo getCategories($q['id']);
                              ?>
                          </table>
                      </div>
                  </div>

              <?php endforeach; ?>
              <?php } ?>
          </div>
          <div class="pagination clearfix">
              <?php $numPages = getNumPages($numQuestions); ?>
              <a href="<?php echo getPageUrl(1); ?>">«</a>
              <?php for ($i = 1; $i <= $numPages; $i++) {
                  if ($i == $page) {
                      echo "<strong>$i</strong>";
                  } else {
                      echo "<a href='" . getPageUrl($i) . "'>$i</a>";
                  }
              } ?>
              <a href="<?php echo getPageUrl($numPages); ?>">»</a>
          </div>
      </div>
      </div>
  </section><!-- End About Section -->

  <section id="featured-services1" class="featured-services">

      <div class="section-title">
          <h2>ارزیابی فنی چک لیست ها</h2>
          <p>...................</p>
          <section class="container" style="background: #f9f9fa; ">
              <form action=""  method="get">
                  <input type="hidden" name="user-id" value="<?php echo $_SESSION['id']; ?>">
                  <button name="submitEchoQuestion" type="submit" class="btn btn-default btn-block"> نمایش سوالات ای که باعث آسیب پذیری سامانه شده است.</button>
              </form>
              <div style="text-align: right" class="qTitle font-type"> لیست سوالات
                  :
              </div>
              <div class="inner content ">
                  <div class="question" id="questionsVulnerable">
                      <?php
                      if (isset($_GET['submitEchoQuestion'])) {
                          $questionsVulnerable=getQuestionAnsweredVulnerable( '1','خیر',$_GET['user-id'] );
                      }
                      foreach ($questionsVulnerable as $q1): ?>

                          <div class="q"><br><br><br><span
                                      class="i">+</span><?php echo "سوال شماره ی" . $q1['id'] . " : ", "<br>" . $q1['text'] ?>
                          </div>

                          <div style="display:block" class="r">
                              <div style="text-align:right;font-family:B Nazanin" class="a" ><?php echo "پاسخ شماره ی " .$q1['id1']." : ","".$q1['rid1']." ". $q1['text1'] ?> </div>
                          </div>
                      <?php endforeach; ?>

                      <?php
                      if (isset($_GET['submitEchoQuestion'])) {
                          $questionsVulnerable=getQuestionAnsweredVulnerable('0','بله',$_GET['user-id'] );  }
                      foreach ($questionsVulnerable as $q2): ?>
                          <div class="q"><br><br><br><span
                                      class="i">+</span><?php echo "سوال شماره ی" . $q2['id'] . " : ", "<br>" . $q2['text'] ?>
                          </div>

                          <div style="display:block" class="r">
                              <div style="text-align:right;font-family:B Nazanin" class="a" ><?php echo "پاسخ شماره ی " .$q2['id1']." : ","".$q2['rid1']." ". $q2['text1'] ?> </div>
                          </div>
                      <?php endforeach; ?>

                      <?php
                      if (isset($_GET['submitEchoQuestion'])) {
                          $getQuestionPublishVulnerable= getQuestionPublishVulnerable($status = 'publish');
                      }
                      foreach ($getQuestionPublishVulnerable as $q3): ?>
                          <div class="q"><br><br><br><span
                                      class="i">+</span><?php echo "سوال شماره ی" . $q3['id'] . " : ", "<br>" . $q3['text'] ?>
                          </div>

                          <div style="display:block" class="r"><?php
                              echo '<div style="text-align: right" class="a">پاسخی برای این سوال داده نشده است !</div>';
                              ?>
                          </div>
                      <?php endforeach; ?>
                  </div>
              </div>


          </section>
          <p>...................</p>
          <div style="background-color:#fafafa;font-family:B Nazanin;direction: ltr">
              <link rel="stylesheet" href="assets/css/chart/styl.css">
              <link rel="stylesheet" href="assets/css/chart/bar.css">

              <div id="jquery-script-menu">
                  <div class="jquery-script-center">
                      <!--<ul>
                      <li><a href="http://www.jqueryscript.net/chart-graph/HTML-Table-Based-Column-Chart-Plugin-For-jQuery-graph-js.html">Download This Plugin</a></li>
                      <li><a href="http://www.jqueryscript.net/">Back To jQueryScript.Net</a></li>
                      </ul>-->
                      <div class="jquery-script-ads">
                          <script type="text/javascript"><!--
                              google_ad_client = "ca-pub-2783044520727903";
                              /* jQuery_demo */
                              google_ad_slot = "2780937993";
                              google_ad_width = 728;
                              google_ad_height = 90;
                              //-->

                          </script>
                      </div>
                      <div class="jquery-script-clear"></div>
                  </div>
              </div>
              <!--  <h1 style="margin:150px auto 30px auto; text-align:center;">jQuery graph.js Plugin Demo</h1>-->

              <div class="pure-g container">
                  <div class="pure-u-1-2">
                      <div class="container" id="wrapper">
                          <div class="chart">
                              <h3>ارزیابی امنیتی سامانه مورد نظر</h3>
                              <table id="data-table" border="1" cellpadding="10" cellspacing="0"
                                     summary="Percentage of knowledge acquired during my experience
            for each technology or language.">
                                  <style>
                                      .btn-group button {
                                          background-color: #04AA6D; /* Green background */
                                          border: 1px solid green; /* Green border */
                                          color: white; /* White text */
                                          padding: 10px 24px; /* Some padding */
                                          cursor: pointer; /* Pointer/hand icon */
                                          float: left; /* Float the buttons side by side */
                                      }

                                      /* Clear floats (clearfix hack) */
                                      .btn-group:after {
                                          content: "";
                                          clear: both;
                                          display: table;
                                      }

                                      .btn-group button:not(:last-child) {
                                          border-right: none; /* Prevent double borders */
                                      }

                                      /* Add a background color on hover */
                                      .btn-group button:hover {
                                          background-color: #3e8e41;
                                      }
                                  </style>

                                  <div class="btn-group">
                                      <button>بخش نرم افزار</button>
                                      <button>بخش سخت افزار</button>
                                      <button>بخش شبکه</button>
                                  </div>
                                  <thead>
                                  <tr>
                                      <td>&nbsp;</td>
                                      <th scope="col"></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <?php
                                          echo $countPendingQuestion;
                                          ?>

                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>

                                      <td>

                                          <?php
                                          echo $countPublishQuestionSoftware;

                                          ?>


                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <!-- view=1 and rid1= yes-->
                                          <?php
                                          $AnswersYesView1Question = getcountAnswersYesView1Question('(حمله فینی)', $_SESSION['id']) + getcountAnswersYesView1Question('(حمله رقابتی)', $_SESSION['id'])
                                              + getcountAnswersYesView1Question('(حمله تاریخ جایگزین)', $_SESSION['id']) + getcountAnswersYesView1Question('(حمله 51 درصد)', $_SESSION['id'])
                                              + getcountAnswersYesView1Question('(حمله شکل پذیری تراکنش)', $_SESSION['id']) + getcountAnswersYesView1Question('(حمله خسوف)', $_SESSION['id'])
                                              + getcountAnswersYesView1Question('(آسیب پذیری در کد منبع قرارداد)', $_SESSION['id']) + getcountAnswersYesView1Question('(آسیب پذیری در ماشین مجازی)', $_SESSION['id'])
                                              + getcountAnswersYesView1Question('(استخراج خودخواهانه)', $_SESSION['id']) + getcountAnswersYesView1Question('(فورک پس از مضایقه)', $_SESSION['id']);

                                          ?>
                                          <!-- view=0 and rid1= no-->
                                          <?php
                                          $AnswersNoView0Question = getcountAnswersNoView0Question('(حمله فینی)', $_SESSION['id']) + getcountAnswersNoView0Question('(حمله رقابتی)', $_SESSION['id'])
                                              + getcountAnswersNoView0Question('(حمله تاریخ جایگزین)', $_SESSION['id']) + getcountAnswersNoView0Question('(حمله 51 درصد)', $_SESSION['id'])
                                              + getcountAnswersNoView0Question('(حمله شکل پذیری تراکنش)', $_SESSION['id']) + getcountAnswersNoView0Question('(حمله خسوف)', $_SESSION['id'])
                                              + getcountAnswersNoView0Question('(آسیب پذیری در کد منبع قرارداد)', $_SESSION['id']) + getcountAnswersNoView0Question('(آسیب پذیری در ماشین مجازی)', $_SESSION['id'])
                                              + getcountAnswersNoView0Question('(استخراج خودخواهانه)', $_SESSION['id']) + getcountAnswersNoView0Question('(فورک پس از مضایقه)', $_SESSION['id']);

                                          ?>
                                          <!--secur system part software-->
                                          <?php
                                          $secursystemSoftware = $AnswersYesView1Question + $AnswersNoView0Question;
                                          $total = $countPublishQuestionSoftware;
                                          echo get_percentage($total, $secursystemSoftware);

                                          ?>

                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <!-- view=1 and rid1= no-->
                                          <?php
                                          $AnswersNoView1Question = getcountAnswersNoView1Question('(حمله فینی)', $_SESSION['id']) + getcountAnswersNoView1Question('(حمله رقابتی)', $_SESSION['id'])
                                              + getcountAnswersNoView1Question('(حمله تاریخ جایگزین)', $_SESSION['id']) + getcountAnswersNoView1Question('(حمله 51 درصد)', $_SESSION['id'])
                                              + getcountAnswersNoView1Question('(حمله شکل پذیری تراکنش)', $_SESSION['id']) + getcountAnswersNoView1Question('(حمله خسوف)', $_SESSION['id'])
                                              + getcountAnswersNoView1Question('(آسیب پذیری در کد منبع قرارداد)', $_SESSION['id']) + getcountAnswersNoView1Question('(آسیب پذیری در ماشین مجازی)', $_SESSION['id'])
                                              + getcountAnswersNoView1Question('(استخراج خودخواهانه)', $_SESSION['id']) + getcountAnswersNoView1Question('(فورک پس از مضایقه)', $_SESSION['id']);
                                          ?>
                                          <!-- view=0 and rid1= yes-->
                                          <?php
                                          $AnswersYesView0Question = getcountAnswersYesView0Question('(حمله فینی)', $_SESSION['id']) + getcountAnswersYesView0Question('(حمله رقابتی)', $_SESSION['id'])
                                              + getcountAnswersYesView0Question('(حمله تاریخ جایگزین)', $_SESSION['id']) + getcountAnswersYesView0Question('(حمله 51 درصد)', $_SESSION['id'])
                                              + getcountAnswersYesView0Question('(حمله شکل پذیری تراکنش)', $_SESSION['id']) + getcountAnswersYesView0Question('(حمله خسوف)', $_SESSION['id'])
                                              + getcountAnswersYesView0Question('(آسیب پذیری در کد منبع قرارداد)', $_SESSION['id']) + getcountAnswersYesView0Question('(آسیب پذیری در ماشین مجازی)', $_SESSION['id'])
                                              + getcountAnswersYesView0Question('(استخراج خودخواهانه)', $_SESSION['id']) + getcountAnswersYesView0Question('(فورک پس از مضایقه)', $_SESSION['id']);
                                          ?>
                                          <!--$vulnarable system part software-->
                                          <!-- سوال های پاسخ داده نشده را هم جزء آسیب پذیری گذاشتم-->
                                          <?php
                                          $vulnarablesystemSoftware = $AnswersNoView1Question + $AnswersYesView0Question + $countPublishQuestionSoftware;
                                          $total = $countPublishQuestionSoftware;
                                          echo get_percentage($total, $vulnarablesystemSoftware);

                                          ?>


                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td><?php
                                          $countPublishQuestionHardware = getcountStatusQuestion('(کیف پول سرد)', 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion('(کیف پول گرم)', 'publish', $_SESSION['id']) + getcountStatusQuestion('(الگوریتم های اجماع)', 'publish', $_SESSION['id']);
                                          echo $countPublishQuestionHardware;

                                          ?></td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <!-- view=1 and rid1= yes-->
                                          <?php
                                          $AnswersYesView1Question = getcountAnswersYesView1Question('(کیف پول سرد)', $_SESSION['id'])
                                              + getcountAnswersYesView1Question('(کیف پول گرم)', $_SESSION['id']) + getcountAnswersYesView1Question('(الگوریتم های اجماع)', $_SESSION['id']);
                                          ?>
                                          <!-- view=0 and rid1= no-->
                                          <?php
                                          $AnswersNoView0Question = getcountAnswersNoView0Question('(کیف پول سرد)', $_SESSION['id'])
                                              + getcountAnswersNoView0Question('(کیف پول گرم)', $_SESSION['id']) + getcountAnswersNoView0Question('(الگوریتم های اجماع)', $_SESSION['id']);
                                          ?>
                                          <!--secur system part hardware-->
                                          <?php
                                          $secursystemHardware = $AnswersYesView1Question + $AnswersNoView0Question;
                                          $total = $countPublishQuestionHardware;
                                          echo get_percentage($total, $secursystemHardware);
                                          ?>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>

                                          <!-- view=1 and rid1= no-->
                                          <?php
                                          $AnswersNoView1Question = getcountAnswersNoView1Question('(کیف پول سرد)', $_SESSION['id'])
                                              + getcountAnswersNoView1Question('(کیف پول گرم)', $_SESSION['id']) + getcountAnswersNoView1Question('(الگوریتم های اجماع)', $_SESSION['id']);
                                          ?>
                                          <!-- view=0 and rid1= yes-->
                                          <?php
                                          $AnswersYesView0Question = getcountAnswersYesView0Question('(کیف پول سرد)', $_SESSION['id'])
                                              + getcountAnswersYesView0Question('(کیف پول گرم)', $_SESSION['id']) + getcountAnswersYesView0Question('(الگوریتم های اجماع)', $_SESSION['id']);
                                          ?>


                                          <!--$vulnarable system part hardware-->
                                          <!--سوال های پاسخ داده نشده-->
                                          <?php
                                          $countPublishQuestionHardware = getcountStatusQuestion('(کیف پول سرد)', 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion('(کیف پول گرم)', 'publish', $_SESSION['id']) + getcountStatusQuestion('(الگوریتم های اجماع)', 'publish', $_SESSION['id']);

                                          $vulnarablesystem = $AnswersNoView1Question + $AnswersYesView0Question + $countPublishQuestionHardware;
                                          $total = $countPublishQuestionHardware;
                                          echo get_percentage($total, $vulnarablesystem);
                                          ?>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td><?php
                                          $countPublishQuestionNetwork = getcountStatusQuestion('(امضاهای آسیب پذیر)', 'publish', $_SESSION['id']) + getcountStatusQuestion("(ایجاد کلید ناقص)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله منع سرویس توزیع شده)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله زمان ربایی)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله مسیریابی)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله سیبیل)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله فیشینگ)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله لغت نامه)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله باز پخش)", 'publish', $_SESSION['id']);

                                          echo $countPublishQuestionNetwork;

                                          ?></td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <!-- view=1 and rid1= yes-->
                                          <?php
                                          $AnswersYesView1Question = getcountAnswersYesView1Question('(امضاهای آسیب پذیر)', $_SESSION['id']) + getcountAnswersYesView1Question("(ایجاد کلید ناقص)", $_SESSION['id'])
                                              + getcountAnswersYesView1Question("(حمله منع سرویس توزیع شده)", $_SESSION['id']) + getcountAnswersYesView1Question("(حمله زمان ربایی)", $_SESSION['id'])
                                              + getcountAnswersYesView1Question("(حمله مسیریابی)", $_SESSION['id']) + getcountAnswersYesView1Question("(حمله سیبیل)", $_SESSION['id'])
                                              + getcountAnswersYesView1Question("(حمله فیشینگ)", $_SESSION['id']) + getcountAnswersYesView1Question("(حمله لغت نامه)", $_SESSION['id'])
                                              + getcountAnswersYesView1Question("(حمله باز پخش)", $_SESSION['id']);
                                          ?>
                                          <!-- view=0 and rid1= no-->
                                          <?php
                                          $AnswersNoView0Question = getcountAnswersNoView0Question('(امضاهای آسیب پذیر)', $_SESSION['id']) + getcountAnswersNoView0Question("(ایجاد کلید ناقص)", $_SESSION['id'])
                                              + getcountAnswersNoView0Question("(حمله منع سرویس توزیع شده)", $_SESSION['id']) + getcountAnswersNoView0Question("(حمله زمان ربایی)", $_SESSION['id'])
                                              + getcountAnswersNoView0Question("(حمله مسیریابی)", $_SESSION['id']) + getcountAnswersNoView0Question("(حمله سیبیل)", $_SESSION['id'])
                                              + getcountAnswersNoView0Question("(حمله فیشینگ)", $_SESSION['id']) + getcountAnswersNoView0Question("(حمله لغت نامه)", $_SESSION['id'])
                                              + getcountAnswersNoView0Question("(حمله باز پخش)", $_SESSION['id']); ?>
                                          <!--secur system part network-->
                                          <?php
                                          $secursystemNetwork = $AnswersYesView1Question + $AnswersNoView0Question;
                                          $total = $countPublishQuestionNetwork;
                                          echo get_percentage($total, $secursystemNetwork);
                                          ?>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope="row"></th>
                                      <td>
                                          <!-- view=1 and rid1= no-->
                                          <?php
                                          $AnswersNoView1Question = getcountAnswersNoView1Question('(امضاهای آسیب پذیر)', $_SESSION['id']) + getcountAnswersNoView1Question("(ایجاد کلید ناقص)", $_SESSION['id'])
                                              + getcountAnswersNoView1Question("(حمله منع سرویس توزیع شده)", $_SESSION['id']) + getcountAnswersNoView1Question("(حمله زمان ربایی)", $_SESSION['id'])
                                              + getcountAnswersNoView1Question("(حمله مسیریابی)", $_SESSION['id']) + getcountAnswersNoView1Question("(حمله سیبیل)", $_SESSION['id'])
                                              + getcountAnswersNoView1Question("(حمله فیشینگ)", $_SESSION['id']) + getcountAnswersNoView1Question("(حمله لغت نامه)", $_SESSION['id'])
                                              + getcountAnswersNoView1Question("(حمله باز پخش)", $_SESSION['id']); ?>

                                          <!-- view=0 and rid1= yes-->
                                          <?php
                                          $AnswersYesView0Question = getcountAnswersYesView0Question('(امضاهای آسیب پذیر)', $_SESSION['id']) + getcountAnswersYesView0Question("(ایجاد کلید ناقص)", $_SESSION['id'])
                                              + getcountAnswersYesView0Question("(حمله منع سرویس توزیع شده)", $_SESSION['id']) + getcountAnswersYesView0Question("(حمله زمان ربایی)", $_SESSION['id'])
                                              + getcountAnswersYesView0Question("(حمله مسیریابی)", $_SESSION['id']) + getcountAnswersYesView0Question("(حمله سیبیل)", $_SESSION['id'])
                                              + getcountAnswersYesView0Question("(حمله فیشینگ)", $_SESSION['id']) + getcountAnswersYesView0Question("(حمله لغت نامه)", $_SESSION['id'])
                                              + getcountAnswersYesView0Question("(حمله باز پخش)", $_SESSION['id']); ?>

                                          <!--$vulnarable system part network-->
                                          <?php
                                          $countPublishQuestionNetwork = getcountStatusQuestion('(امضاهای آسیب پذیر)', 'publish', $_SESSION['id']) + getcountStatusQuestion("(ایجاد کلید ناقص)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله منع سرویس توزیع شده)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله زمان ربایی)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله مسیریابی)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله سیبیل)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله فیشینگ)", 'publish', $_SESSION['id']) + getcountStatusQuestion("(حمله لغت نامه)", 'publish', $_SESSION['id'])
                                              + getcountStatusQuestion("(حمله باز پخش)", 'publish', $_SESSION['id']);

                                          $vulnarablesystemNetwork = $AnswersNoView1Question + $AnswersYesView0Question + $countPublishQuestionNetwork;
                                          $total = $countPublishQuestionNetwork;
                                          echo get_percentage($total, $vulnarablesystemNetwork);
                                          ?>
                                      </td>
                                  </tr>

                                  </tbody>
                              </table>
                              <style>
                                  .bar.fig0 {
                                      background: #f5cd07;
                                  }

                                  .bar.fig2 {
                                      background: #0aea15;
                                  }

                                  .bar.fig3 {
                                      background: #f80622;
                                  }

                                  .bar.fig4 {
                                      background: DodgerBlue;
                                  }

                                  .bar.fig5 {
                                      background: #0aea15;
                                  }

                                  .bar.fig6 {
                                      background: #f80622;
                                  }

                                  .bar.fig7 {
                                      background: DodgerBlue;
                                  }

                                  .bar.fig8 {
                                      background: #0aea15;
                                  }

                                  .bar.fig9 {
                                      background: #f80622;
                                  }

                                  .bar span {
                                      color: #0a0a0a;
                                  }

                                  .y-axis {
                                      width: 83%;
                                  }

                                  .section-title span {
                                      font-size: large;
                                  }

                                  .bar {
                                      margin-left: 14px;
                                  }

                              </style>
                          </div>
                      </div>
                  </div>
                  <div class="pure-u-1-2">
                      <style>
                          .btn1, .btn2, .btn3, .btn4 {
                              background-color: #f5cd07;
                              border: none;
                              color: white;
                              padding: 12px 16px;
                              font-size: 16px;
                              cursor: pointer;
                              margin-right: -344px;
                          }

                          .btn2 {
                              background-color: DodgerBlue !important;
                          }

                          .btn3 {
                              background-color: #0aea15 !important;
                          }

                          .btn4 {
                              background-color: #f80622 !important;
                          }
                      </style>
                      <div>
                          <label style=""> کل چک لیست های منتظر تایید:</label>
                          <button style="margin-right:-406px" class="btn1"></button>
                          <br>
                          <label style="">چک لیست های پاسخ داده نشده:</label>
                          <button style="margin-right:-408px" class=" btn2 "></button>
                          <br>
                          <label style="">امنیت سامانه شما :</label>
                          <button style="margin-right:-470px" class="btn3"></button>
                          <br>
                          <label style="">آسیب پذیری سامانه شما :</label>
                          <button style="margin-right:-440px" class="btn4"></button>
                      </div>
                      <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
                      <script src="assets/js/chart/graph.js"></script>
                      <script type="text/javascript">

                          var _gaq = _gaq || [];
                          _gaq.push(['_setAccount', 'UA-36251023-1']);
                          _gaq.push(['_setDomainName', 'jqueryscript.net']);
                          _gaq.push(['_trackPageview']);

                          (function () {
                              var ga = document.createElement('script');
                              ga.type = 'text/javascript';
                              ga.async = true;
                              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                              var s = document.getElementsByTagName('script')[0];
                              s.parentNode.insertBefore(ga, s);
                          })();
                      </script>
                  </div>
              </div>
          </div>
  </section>
  <!-- ======= Footer ======= -->
  <footer id="footer">
      <!--form input and remove-->
      <style>
          label,h1{
              font-family:B Nazanin;
          }
          .open-button {
              background-color: #555;
              color: white;
              padding: 16px 20px;
              border: none;
              cursor: pointer;
              opacity: 0.8;
              position: fixed;
              bottom: 64px;
              right: 8px;
              width: 100px;
          }

          /* The popup form - hidden by default */
          .form-popup {
              display: none;
              position: fixed;
              bottom: 0;
              right: 15px;
              border: 3px solid #f1f1f1;
              z-index: 996;
          }

          /* Add styles to the form container */
          .form-container {
              max-width: 300px;
              padding: 10px;
              background-color: white;
          }

          /* Full-width input fields */
          .form-container input[type=text], .form-container input[type=password] {
              width: 100%;
              padding: 15px;
              margin: 5px 0 22px 0;
              border: none;
              background: #f1f1f1;
          }

          /* When the inputs get focus, do something */
          .form-container input[type=text]:focus, .form-container input[type=password]:focus {
              background-color: #ddd;
              outline: none;
          }

          /* Set a style for the submit/login button */
          .form-container .btn {
              background-color: #04AA6D;
              color: white;
              padding: 16px 20px;
              border: none;
              cursor: pointer;
              width: 100%;
              margin-bottom:10px;
              opacity: 0.8;
          }

          /* Add a red background color to the cancel button */
          .form-container .cancel {
              background-color: red;
          }

          /* Add some hover effects to buttons */
          .form-container .btn:hover, .open-button:hover {
              opacity: 1;
          }
      </style>

      <button class="open-button  back-to-top d-flex align-items-center justify-content-center active"
              onclick="openForm()">ثبت / حذف پاسخ
      </button>

      <div class="form-popup" id="myForm" style="background: white">

          <form action="" method="post" class="pure-form searchform font-type">
              <h4>ثبت پاسخ</h4>
              <input id="quantity" type="number" name="qid" min="1" max="240" placeholder="شماره سوال را وارد کنید:" required style="width:100%"><br>

              <select class="font-type" name="rid1"  style="width:100%" p>
                  <option label="گزینه مورد نظر را انتخاب کنید:" ></option>
                  <option value="بله">بله</option>
                  <option value="خیر">خیر</option>
              </select>
              <input type="text" name="uAnswered" placeholder="توضیحات لازم را وارد کنید:" required style="width:100%">

              <input type="hidden" name="uid" value="<?php echo $_SESSION['id']; ?>">

              <button style="width:100%" type="submit" name="submitAnswered" class="pure-button button-green font-type" value="ارسال پاسخ">ارسال پاسخ
              </button>
          </form>
          <p style="text-align: center">...................</p>
          <h4>حذف پاسخ</h4>
          <form action="" method="post"  class="pure-form searchform font-type">
              <input style="width:100%" type="number" id="lname" name="text" min="1" max="240"
                     placeholder="شماره پاسخ را وارد کنید">
              <input type="hidden" name="uid" value="<?php echo $_SESSION['id']; ?>">
              <button style="width:100%" type="submit" name="removeAnswer" class="btn btn-danger" value="حذف پاسخ">حذف
                  پاسخ
              </button></form>
          <p style="text-align: center">...................</p>
          <form id="form" action=" " method="get" class="pure-form searchform font-type">
              <input style="width:100%" type="number" id="quantity" name="numberQuestion" min="1" max="240" placeholder="شماره سوال مورد نظر را وارد کنید:">
              <input style="width:100%" class="pure-button button-green font-type" type="submit" value="جستجو">
          </form>
          <button style="width:100%" type="button" class="btn cancel btn-danger" onclick="closeForm()">انصراف</button>

      </div>


      <script>
          function openForm() {
              document.getElementById("myForm").style.display = "block";
          }

          function closeForm() {
              document.getElementById("myForm").style.display = "none";
          }
      </script>
      <!--end form input and remove-->
      <div class="container footer-bottom clearfix">
          <div class="copyright">
              &copy; کپی رایت <strong><span>دانشگاه صنعتی مالک اشتر(تهران)</span></strong> تمامی حقوق محفوظ است.
          </div>
          <div class="credits">
              <!-- All the links in the footer should remain intact. -->
              <!-- You can delete the links only if you purchased the pro version. -->
              <!-- Licensing information: https://bootstrapmade.com/license/ -->
              <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
              طراحی شده توسط: <a href="https://mail.google.com/mail/m.datkhah@gmail.com/">مهندس محمد دادخواه</a>
          </div>
      </div>
  </footer>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
  <!--codes project base-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/scripts.js"></script>
  <?php if (isAdmin()): ?>
      <script src="assets/js/admin.js"></script>
  <?php endif; ?>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>